# Imports

from ..logs import logger

from argparse import ArgumentParser, RawDescriptionHelpFormatter
import logging
import os
from pprint import pprint

from myninjas.shell import abort, EXIT_FAILURE, EXIT_SUCCESS, EXIT_UNKNOWN
# from myninjas.logging import LoggingHelper
import sys
from .subcommands import check_command, deploy_command, info_command, provision_command
from ..constants import LOGGER_NAME
from ..contexts import initialize_context
from .initialize import SubCommands

# log = LoggingHelper(colorize=True, name=LOGGER_NAME, success=True)
# logger = log.setup()
# print("LOGGER", type(logger))

# Commands


def main_command():
    """Provision and deployment."""

    __author__ = "Shawn Davis <shawn@develmaycare.com>"
    __date__ = "2018-11-15"
    __help__ = """NOTES

TODO

    """
    __version__ = "0.9.1-d"

    parser = ArgumentParser(description=__doc__, epilog=__help__, formatter_class=RawDescriptionHelpFormatter)

    # Initialize sub-commands.
    subparsers = parser.add_subparsers(
        dest="subcommand",
        help="Commands",
        metavar="check, deploy, info, provision"
    )

    commands = SubCommands(subparsers)
    commands.check()
    commands.deploy()
    commands.info()
    commands.provision()

    # Access to the version number requires special consideration, especially
    # when using sub parsers. The Python 3.3 behavior is different. See this
    # answer: http://stackoverflow.com/questions/8521612/argparse-optional-subparser-for-version
    parser.add_argument(
        "-v",
        action="version",
        help="Show version number and exit.",
        version=__version__
    )

    parser.add_argument(
        "--version",
        action="version",
        help="Show verbose version information and exit.",
        version="%(prog)s" + " %s %s by %s" % (__version__, __date__, __author__)
    )

    # Parse the given arguments.
    args = parser.parse_args()
    command = args.subcommand

    if args.debug_enabled:
        logger.setLevel(logging.DEBUG)
        logger.debug(str(args))

    # Set and check the path to command meta data.
    if args.meta_path:
        meta_path = args.meta_path
    else:
        meta_path = "deployable"

    logger.debug("Meta path: %s" % os.path.abspath(meta_path))

    if not os.path.exists(meta_path):
        logger.warning("Path to meta data does not exist: %s" % meta_path)
        sys.exit(EXIT_FAILURE)

    # Initialize the global context.
    context = initialize_context(args.environment, path=meta_path, variables_from_command_line=args.variables)
    # pprint(context.mapping())
    # sys.exit()

    if command == "check":
        success = check_command(
            args.environment,
            context=context,
            path=meta_path,
            preview=args.preview_enabled
        )
    elif command == "deploy":
        success = deploy_command(
            args.environment,
            context=context,
            full_preview=args.full_preview_enabled,
            path=meta_path,
            preview=args.preview_enabled,
            release=args.release,
            tags=args.tags
        )
    elif command == "info":
        success = info_command(
            args.environment,
            context=context,
            output_file=args.output_file,
            output_format=args.output_format,
            path=meta_path,
            release=args.release,
            scope=args.scope,
            tags=args.tags
        )
    elif command == "provision":
        success = provision_command(
            args.environment,
            context=context,
            full_preview=args.full_preview_enabled,
            path=meta_path,
            preview=args.preview_enabled,
            tags=args.tags
        )
    else:
        logger.warning("Command not implemented: %s" % command)
        success = False

    # Quit.
    if success:
        sys.exit(EXIT_SUCCESS)
    else:
        sys.exit(EXIT_FAILURE)
