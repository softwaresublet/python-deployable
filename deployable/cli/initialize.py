# Imports

import logging
import os
from ..constants import ENVIRONMENT_BASE, LOGGER_NAME as DEFAULT_LOGGER_NAME, SCOPE_ALL, SCOPE_DEPLOY
from ..contexts import Context
from ..hosts import factory as host_factory
# from ..roles import factory as role_factory
from ..variables import factory as variable_factory, Variable
from ..utils import filter_objects

LOGGER_NAME = os.environ.get("PYTHON_DEPLOYABLE_LOGGER_NAME", DEFAULT_LOGGER_NAME)

logger = logging.getLogger(LOGGER_NAME)

# Functions

# see context.initialize_context
# def initialize_context(environment, path="deployable", variables_from_command_line=None):
#     """Initialize the global context.
#
#     This processes the variables passed from the command line as well as the ``variables.ini`` file if one has been
#     provided.
#
#     :rtype: Context
#
#     """
#     context = Context()
#
#     # Parse variables provided at the command line.
#     if variables_from_command_line is not None:
#         for spec in variables_from_command_line:
#             name, value = spec.split(":")
#
#             name = name.strip()
#             value = value.strip()
#
#             context.add(
#                 name,
#                 comment="%s initialized from command line." % name,
#                 value=value
#             )
#
#     # Load variables from the INI file.
#     variables = list()
#     variables_path = os.path.join(path, "_variables.ini")
#     if os.path.exists(variables_path):
#         logger.info("Load variables from %s" % variables_path)
#         _variables = variable_factory(variables_path)
#
#         variables = filter_objects(
#             _variables,
#             environments=[ENVIRONMENT_BASE, environment]
#         )
#
#     # Add variables to the context.
#     logger.debug("Globals: %s" % variables)
#     for v in variables:
#         context.append(v)
#
#     return context

# Classes


class SubCommands(object):
    """A utility class which keeps the ``cli`` package clean."""

    def __init__(self, subparsers):
        self.subparsers = subparsers

    def check(self):
        """Initialize the check sub-command."""
        sub = self.subparsers.add_parser(
            "check",
            help="Check connections."
        )

        sub.add_argument(
            "environment",
            help="The environment to which the deploy will occur."
        )

        self._add_common_options(sub)

    def deploy(self):
        """Initialize the deploy sub-command."""
        sub = self.subparsers.add_parser(
            "deploy",
            help="Work with deployment data."
        )

        sub.add_argument(
            "environment",
            help="The environment to which the deploy will occur."
        )

        sub.add_argument(
            "-R="
            "--release=",
            dest="release",
            help="The release identifier. If omitted, the VERSION.txt file will be used."
        )

        self._add_common_options(sub)

    def info(self):
        """Initialize the info sub-command."""
        sub = self.subparsers.add_parser(
            "info",
            help="Export and print deployment or provisioning info."
        )

        sub.add_argument(
            "-c",
            "--color",
            action="store_true",
            dest="color_enabled",
            help="Enable code highlighting for terminal output where appropriate."
        )

        sub.add_argument(
            "-f=",
            "--format=",
            choices=["bash", "html", "md", "plain", "rst"],
            default="plain",
            dest="output_format",
            help="The format of output."
        )

        sub.add_argument(
            "-H=",
            "--host=",
            dest="host",
            help="Output info for a specific host."
        )

        sub.add_argument(
            "-O=",
            "--output-file=",
            dest="output_file",
            help="Output to the given file."
        )

        sub.add_argument(
            "-R="
            "--release=",
            dest="release",
            help="The release identifier. If omitted, the VERSION.txt file will be used."
        )

        sub.add_argument(
            "-s=",
            "--scope=",
            choices=["all", "deploy", "provision", "tenant"],
            default="all",
            dest="scope",
            help="The scope of commands to prepare or run. Default: all"
        )

        self._add_common_options(sub)

    def provision(self):
        """Initialize the provision sub-command."""
        sub = self.subparsers.add_parser(
            "provision",
            help="Work with provisioning data."
        )

        sub.add_argument(
            "environment",
            help="The environment for which provisioning will occur."
        )

        self._add_common_options(sub)

    # noinspection PyMethodMayBeStatic
    def _add_common_options(self, subcommand):
        """Add the common switches to a given sub-command instance.

        :param subcommand: The sub-command instance.

        """

        subcommand.add_argument(
            "-D",
            "--debug",
            action="store_true",
            dest="debug_enabled",
            help="Enable debug mode. Produces extra output."
        )

        # subcommand.add_argument(
        #     "-e=",
        #     "--env=",
        #     action="append",
        #     dest="environments",
        #     help="The environments for which commands will be prepared or executed. This option may be used multiple "
        #          "times. The base environment is included by default."
        # )

        subcommand.add_argument(
            "-P=",
            "--path=",
            # default=SCOPE_DEPLOY,
            dest="meta_path",
            help="The path to the location of meta files. Default: ./deployable"
        )

        subcommand.add_argument(
            "-p",
            action="store_true",
            dest="preview_enabled",
            help="Preview the commands (comments) to be executed."
        )

        subcommand.add_argument(
            "--preview",
            action="store_true",
            dest="full_preview_enabled",
            help="Preview the commands (comments and commands) to be executed."
        )

        # subcommand.add_argument(
        #     "-s=",
        #     "--scope=",
        #     choices=["deploy", "provision", "tenant"],
        #     default=SCOPE_DEPLOY,
        #     dest="scope",
        #     help="The scope of commands to prepare or run. Default: %s" % SCOPE_DEPLOY
        # )

        subcommand.add_argument(
            "-t=",
            "--tag=",
            action="append",
            dest="tags",
            help="Filter commands for the given tag. This option may be used more than once."
        )

        subcommand.add_argument(
            "-V=",
            "--variable=",
            action="append",
            dest="variables",
            help='Specify variable overrides in the form of "name:value". Use quotes as needed. This option may be '
                 'used multiple times.'
        )
