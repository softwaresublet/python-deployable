# Imports

from datetime import datetime
import logging
from myninjas.utils import read_file
import os
# from ..commands import Command
from ..constants import ENVIRONMENT_BASE, LOGGER_NAME
from ..contexts import Context
from ..deployments import Deploy
from ..discovery import Discovery
from ..hosts import factory as host_factory
from ..roles import factory as role_factory
from ..utils import filter_objects
from ..ssh import Client

logger = logging.getLogger(LOGGER_NAME)

# Helpers


def execute_commands(context, environment, host, path, full_preview=False, preview=False, tags=None):
    """Execute (or just preview) commands for a given host.

    :param context: The context to use for processing commands.
    :type context: Context

    :param environment: The operational environment used to filter commands.
    :type environment: str

    :param host: The host on which commands will be executed.
    :type host: Host

    :param path: The path to meta data.
    :type path: str

    :param full_preview: Preview the actual commands to be executed.
    :type full_preview: bool

    :param preview: Preview the commands (comments) to be executed.
    :type preview: bool

    :param tags: A list of tags used to filter the commands.
    :type tags: list[str]

    :rtype: bool

    """
    # Initialize the SSH client to be used for remote execution (below).
    client = Client(host)

    # Get the roles for the current host.
    roles = list()
    for role_name in host.roles:
        role = role_factory(role_name, context=context, path=path)
        if role is None:
            logger.warning("Unrecognized role for %s: %s" % (host.name, role_name))
            continue

        roles.append(role)

    # Iterate through the host's roles.
    for role in roles:

        # Filter the commands to be executed based on environment. The steps have already been filtered by scope by
        # their location in a deploy/<role_name>/ directory.
        commands = filter_objects(
            role.steps,
            environments=[ENVIRONMENT_BASE, environment],
            tags=tags
        )

        # Execute (or preview) the commands for the host.
        for command in commands:

            # Set the client attribute for remote commands.
            if not command.is_local:
                command.client = client

            # Always send the command identifier (comment).
            logger.info(command.comment)

            # Handle previews.
            if full_preview:
                print(command.preview())
                print("")

            if preview or full_preview:
                continue

            # TODO: Write steps to a retry file so we can pick up where a failing step has left off.

            # Execute the command.
            start_dt = datetime.now()
            if command.run():
                end_dt = datetime.now()
                delta = end_dt - start_dt

                # noinspection PyUnresolvedReferences
                logger.success("Command completed in %s seconds." % delta.seconds)
                logger.debug(command.output)
            else:
                if command.stop:
                    logger.critical(command.comment)
                    logger.debug("Command in the %s role for the %s host has failed." % (role.name, host.name))
                    logger.debug(command.error)
                    return False
                else:
                    logger.error(command.comment)
                    logger.debug("Command in the %s role for the %s host has failed." % (role.name, host.name))
                    logger.error(command.preview())
                    logger.debug(command.error)

    # # Filter the commands to be executed based on environment. The steps have already been filtered by scope by
    # # their location in a deploy/<role_name>/ directory.
    # commands = list()
    # for role in roles:
    #     steps = filter_objects(
    #         role.steps,
    #         environments=[ENVIRONMENT_BASE, environment],
    #     )
    #     commands += steps
    #
    # # Execute (or just preview) commands for the host.
    # for command in commands:
    #
    #     logger.info(command.comment)
    #
    #     if full_preview:
    #         print(command.preview())
    #         print("")
    #
    #     if preview or full_preview:
    #         continue
    #
    #     remote = Command(client, command)
    #     start_dt = datetime.now()
    #     if remote.run():
    #         end_dt = datetime.now()
    #         delta = end_dt - start_dt
    #
    #         logger.success("Command completed in %s seconds." % delta.seconds)
    #         logger.debug(remote.output)
    #     else:
    #         if command.stop:
    #             logger.critical(command.comment)
    #             logger.debug(remote.error)
    #             return False
    #         else:
    #             logger.error(command.comment)
    #             logger.error(command.preview())
    #             logger.debug(remote.error)


def execute_steps(context, environment, host, path, full_preview=False, preview=False, tags=None):
    # Initialize the SSH client to be used for remote execution (below).
    client = Client(host)

    # Get the roles for the current host.
    roles = list()
    for role_name in host.roles:
        role = role_factory(role_name, context=context, path=path)
        if role is None:
            continue

        roles.append(role)

    # Iterate through the found roles.
    for role in roles:
        logger.info('Processing steps for the "%s" role.' % role.name)

        # Filter the steps based on environment. The steps have already been filtered by scope due to their location
        # the deploy or provision directory.
        steps = filter_objects(
            role.steps,
            environments=[ENVIRONMENT_BASE, environment],
            tags=tags
        )

        # Execute (or preview) the commands for each step.
        for step in steps:
            if step.get_count() > 1:
                info = "%s (%s commands)" % (step.name, step.get_count())
            else:
                info = "%s (1 command)" % step.name

            logger.info(info)
            # logger.debug("Step includes these tags: %s" % step.tags)

            # Iterate through the commands.
            count = 1
            for command in step.get_commands():

                # Set the client attribute for remote commands.
                if not command.is_local:
                    command.client = client

                # Handle previews.
                if full_preview:
                    print(command.preview())
                    if count == step.get_count():
                        print("")

                count += 1

                if preview or full_preview:
                    continue

                # TODO: Write steps to a retry file so we can pick up where a failing step has left off.

                # Execute the command.
                start_dt = datetime.now()
                if command.run():
                    end_dt = datetime.now()
                    delta = end_dt - start_dt

                    # noinspection PyUnresolvedReferences
                    logger.success("Command completed in %s seconds." % delta.seconds)

                    if command.output:
                        logger.debug(command.output)
                else:
                    error = 'The "%s" command in the "%s" role has failed for the %s host.'
                    if command.stop:
                        logger.critical(error % (command.comment, role.name, host.name))

                        if command.error:
                            logger.debug(command.error)

                        return False
                    else:
                        logger.error(error % (command.comment, role.name, host.name))
                        logger.error(command.preview())

                        if command.error:
                            logger.debug(command.error)


class Output(object):
    """Collect output for the ``info_command()``."""

    def __init__(self, hosts, path=None):
        self.hosts = hosts
        self.path = path
        self._lines = list()

    def to_bash(self):
        lines = list()
        lines.append("#! /bin/bash")
        lines.append("")

        for host in self.hosts:
            pass

    def to_html(self):
        pass

    def to_markdown(self):
        pass

    def to_plain(self):
        for host in self.hosts:
            self._lines.append()

    def to_rst(self):
        pass

# Commands


def check_command(environment, context=None, path="deployable", preview=False):

    # Initialize the context.
    if context is None:
        logger.warning("Initializing empty context. This might be okay. But it probably isn't.")
        context = Context()

    # Add to the context.
    context.add("environment", environments=[environment], value=environment)

    # Load the hosts for the given environment.
    _path = os.path.join(path, "hosts", "%s.ini" % environment)
    if not os.path.exists(_path):
        logger.error("Hosts file for the %s environment does not exist: %s" % (environment, _path))
        return False

    hosts = host_factory(_path)
    if hosts is None:
        logger.warning("No valid hosts were found in: %s" % _path)
        return False

    attributes = {
        'hostname': "Host Name",
        'ip': "IP Address",
        'package_manager': "Package Manager",
        'os': "Operating System",
        'kernel': "Kernel",
        'platform': "Platform",
        'python_version': "Python",
        'hardware': "Hardware",
    }
    for host in hosts:
        print(host.name)
        print("=" * 80)

        discovery = Discovery(host)
        if discovery.load():
            for attribute, label in attributes.items():
                value = getattr(discovery, attribute)
                print("%s: %s" % (label, value))
        else:
            print("Could not connect to host. Bummer.")

        print("")

    return True


def deploy_command(environment, context=None, full_preview=False, name=None, path="deployable", preview=False, release=None, tags=None):
    """Run a deployment.

    :param environment: The environment to which software and assets will be deployed.
    :type environment: str

    :param context: The context to use for the deployment.
    :type context: Context

    :param full_preview: Preview the actual commands to be executed.
    :type full_preview: bool

    :param name: The name of the application or site to be deployed. This defaults to the name of the current working
                 directory. If overridden, it should be suitable for use in a path.
    :type name: str

    :param path: The path to deployment meta data.
    :type path: str

    :param preview: Indicates the deploy should be previewed but not executed.
    :type preview: bool

    :param release: The release identifier. For example: ``1.1``.
    :type release: str

    :param tags: A list of tags used to filter the commands.
    :type tags: list[str]

    :rtype: bool

    .. note::
        It is possible, though unlikely, that the deployment can run without context variables. The ``main_command()``
        will *always* provide a context instance, but programmatically invoking this function may not. If a context is
        *not* provided, an empty context will be instantiated.

    """
    # Get the name of the application or site to be deployed.
    if name is None:
        name = os.path.basename(os.getcwd())
        logger.info("Application/site name is: %s" % name)

    # Get the release identifier.
    if release is None:
        if not os.path.exists("VERSION.txt"):
            logger.error("VERSION.txt file does not exist in the current working directory. Create this file or "
                         "provide a release identifier.")
            return False

        release = read_file("VERSION.txt").strip()

    # It is possible, though unlikely, that the deployment can run without context variables. The main_command() will
    # always provide a context, but programmatically invoking this function may not.
    if context is None:
        logger.warning("Initializing empty context. This might be okay. But it probably isn't.")
        context = Context()

    # Add to the context.
    context.add("environment", environments=[environment], value=environment)
    context.add("local_root", environments=[environment], value=os.path.abspath("."))
    context.add("release", value=release)

    # Load the hosts for the given environment.
    hosts_path = os.path.join(path, "hosts", "%s.ini" % environment)
    if not os.path.exists(hosts_path):
        logger.error("Hosts file for the %s environment does not exist: %s" % (environment, hosts_path))
        return False

    hosts = host_factory(hosts_path)
    if hosts is None:
        logger.warning("No valid hosts were found in: %s" % hosts_path)
        return False

    context.add("hosts", environments=[environment], value=hosts)

    # Create the deploy instance and add it to the context.
    deploy_root = context.get("deploy_root", default=os.path.join("/opt", name))
    deploy = Deploy(environment, deploy_root, release, user=name)
    deploy.start()
    context.add("deploy", value=deploy)

    # print(context.mapping())
    # print(deploy.release_path)
    # exit()

    # Get the roles that may be associated with each host.
    roles_path = os.path.join(path, "deploy")
    for host in hosts:
        context.set("current_host", value=host)
        execute_steps(context, environment, host, roles_path, full_preview=full_preview, preview=preview, tags=tags)

    # Mark the deployment as ended.
    deploy.end()

    return True


def info_command(environment, context=None, host=None, name=None, output_file=None, output_format="plain", path="deployable", release=None, scope=None, tags=None):

    # Load the hosts for the given environment.
    hosts_path = os.path.join(path, "hosts", "%s.ini" % environment)
    if not os.path.exists(hosts_path):
        logger.error("Hosts file for the %s environment does not exist: %s" % (environment, hosts_path))
        return False

    hosts = host_factory(hosts_path)
    if hosts is None:
        logger.warning("No valid hosts were found in: %s" % hosts_path)
        return False

    if host is not None:
        specific_host = None
        for _host in hosts:
            if host == _host.name:
                specific_host = _host

        if specific_host is None:
            logger.error("Could not find specific host: %s" % host)
            return False

        hosts = [specific_host]

    output = Output(hosts, path=output_file)


def provision_command(environment, context=None, full_preview=False, name=None, path="deployable", preview=False, tags=None):
    """Provision one or more hosts to prepare them for deployments.

    :param environment: The environment to which software and assets will be deployed.
    :type environment: str

    :param context: The context to use for the deployment.
    :type context: Context

    :param full_preview: Preview the actual commands to be executed.
    :type full_preview: bool

    :param name: The name of the application or site to be deployed. This defaults to the name of the current working
                 directory. If overridden, it should be suitable for use in a path.
    :type name: str

    :param path: The path to deployment meta data.
    :type path: str

    :param preview: Indicates the deploy should be previewed but not executed.
    :type preview: bool

    :param release: The release identifier. For example: ``1.1``.
    :type release: str

    :param tags: A list of tags used to filter the commands.
    :type tags: list[str]

    :rtype: bool

    .. note::
        It is possible, though unlikely, that the provisioning can run without context variables. The ``main_command()``
        will *always* provide a context instance, but programmatically invoking this function may not. If a context is
        *not* provided, an empty context will be instantiated.

    """
    # Get the name of the application or site to be deployed.
    if name is None:
        name = os.path.basename(os.getcwd())
        logger.info("Application/site name is: %s" % name)

    # It is possible, though unlikely, that the deployment can run without context variables. The main_command() will
    # always provide a context, but programmatically invoking this function may not.
    if context is None:
        logger.warning("Initializing empty context. This might be okay. But it probably isn't.")
        context = Context()

    # Add to the context.
    context.add("environment", environments=[environment], value=environment)

    # Load the hosts for the given environment.
    _path = os.path.join(path, "hosts", "%s.ini" % environment)
    if not os.path.exists(_path):
        logger.error("Hosts file for the %s environment does not exist: %s" % (environment, _path))
        return False

    hosts = host_factory(_path)
    if hosts is None:
        logger.warning("No valid hosts were found in: %s" % _path)
        return False

    context.add("hosts", environments=[environment], value=hosts)

    # Create the deploy instance and add it to the context.
    deploy_root = context.get("deploy_root", default=os.path.join("/opt", name))
    deploy = Deploy(environment, deploy_root, "", user=name)
    context.add("deploy", value=deploy)

    # print(context.mapping())
    # exit()

    # Get the roles that may be associated with each host.
    roles_path = os.path.join(path, "provision")
    for host in hosts:
        context.set("current_host", value=host)
        execute_steps(context, environment, host, roles_path, full_preview=full_preview, preview=preview, tags=tags)

    return True
