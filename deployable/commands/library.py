# Imports

import logging
from paramiko import AuthenticationException, BadHostKeyException, SSHException
# noinspection PyPep8Naming
from socket import error as SocketError
import subprocess
from deployable.constants import ENVIRONMENT_BASE, EXIT_OK, EXIT_UNKNOWN, SCOPE_DEPLOY

logger = logging.getLogger("deployable")

# Exports

__all__ = (
    "Command",
    "Result",
)

# Classes


class Command(object):
    """An individual task (command) to be performed during provisioning or deployment."""

    def __init__(self, statement, **kwargs):
        self.statement = statement

        # The client instance is used for remote() execution, but may never be provided at instantiation because it is
        # always specific to the current host.
        self.client = None

        # Get attributes from keyword arguments.
        self.comment = kwargs.pop("comment", statement)
        self.cwd = kwargs.pop("cwd", None)
        self.environments = kwargs.pop("environments", [ENVIRONMENT_BASE])
        self.is_local = kwargs.pop("local", False)
        self.prefix = kwargs.pop("prefix", None)
        self.scope = kwargs.pop("scope", SCOPE_DEPLOY)
        self.shell = kwargs.pop("shell", None)
        self.stop = kwargs.pop("stop", False)
        self.sudo = kwargs.pop("sudo", True)
        self.tags = kwargs.pop("tags", list())

        # These attributes are collected during as part of execution. See _local() and _remote().
        self.code = None
        self.error = None
        self.output = None

    def __repr__(self):
        return "<%s: %s>" % (self.__class__.__name__, self.comment)

    def __str__(self):
        return self.preview()

    def get_result(self):
        """Get the result instance.

        :rtype: Result

        """
        return Result(self.comment, code=self.code, error=self.error, output=self.output)

    def get_statement(self, cwd=False):
        """Get the command to be executed with prefix and path.

        :param cwd: Indicates whether the path should be included. Generally speaking, you want to include the
                    path to preview the command and omit the path when you want to run the command.
        :type cwd: bool

        :rtype: str

        """
        a = list()

        if self.cwd and cwd:
            a.append("(cd %s &&" % self.cwd)

        if self.prefix:
            a.append("%s &&" % self.prefix)

        if self.sudo:
            if type(self.sudo) is str:
                a.append("sudo -u %s" % self.sudo)
            else:
                a.append("sudo")

        if type(self.statement) is list:
            a += self.statement
        else:
            a.append(self.statement)

        if self.cwd and cwd:
            return " ".join(a) + ")"

        return " ".join(a)

    def preview(self):
        return self.get_statement(cwd=True)

    def run(self):
        """Run the command, local or remote.

        :rtype: bool

        """
        if self.is_local:
            return self._local()

        return self._remote()

    def _local(self):
        """Run the command on the local machine.

        :rtype: bool
        :returns: Returns ``True`` if the command was successful. The ``code`` attribute is also set.

        .. tip::
            Success depends upon the exit code of the command which is not available from all commands on all platforms.
            Check the command's documentation for exit codes and plan accordingly.


        """
        # Prepare to shell output.
        output_stream = subprocess.PIPE
        error_stream = subprocess.PIPE

        statement = self.get_statement()

        # Run the command. Capture output, error, and return code.
        try:
            p = subprocess.Popen(
                statement,
                cwd=self.cwd,
                executable=self.shell,
                shell=True,
                stderr=error_stream,
                stdout=output_stream
            )
            (stdout, stderr) = p.communicate()

            self.code = p.returncode

            a = list()
            for line in str(stderr).split("\\n"):
                a.append(line.strip())

            self.error = "\n".join(a)

            a = list()
            for line in str(stdout).split("\\n"):
                a.append(line.strip())

            self.output = "\n".join(a)
        except Exception as e:
            self.code = EXIT_UNKNOWN
            self.error = str(e)

        return self.code == EXIT_OK

    def _remote(self):
        """Run the command using a client connection.

        :rtype: bool

        .. note::
            The ``client`` instance must be assigned to the command instance prior to calling this method.

        """
        if self.client is None:
            logger.error('"%s" called without a client instance.' % self.comment)
            return False

        try:
            self.client.connect()
        except (AuthenticationException, BadHostKeyException, SSHException, SocketError) as e:
            self.error = str(e)
            return False

        statement = self.get_statement(cwd=True)
        try:
            stdin, stdout, stderr = self.client.exec_command(statement)
            self.code = stdout.channel.recv_exit_status()

            a = list()
            for line in stderr.readlines():
                a.append(line.strip())

            self.error = "\n".join(a)

            a = list()
            for line in stdout.readlines():
                a.append(line.strip())

            self.output = "\n".join(a)
        except SSHException as e:
            self.error = str(e)

        self.client.close()

        if self.code != 0:
            return False

        return True


class Result(object):
    """The result of command execution."""

    def __init__(self, name, code=EXIT_OK, error=None, output=None):
        """Initialize a result.

        :param name: The name of the result, typically the command comment.
        :type name: str

        :param code: The exit code.
        :type code: int

        :param error: The error message, if any.
        :type error: str

        :param output: The command's output, if any.
        :type output: str

        """
        self.code = code
        self.error = error
        self.name = name
        self.output = output

    @property
    def failure(self):
        """Indicates the command failed.

        :rtype: bool

        """
        return self.code != EXIT_OK

    @property
    def success(self):
        """Indicates the command succeeded.

        :rtype: bool

        """
        return self.code == EXIT_OK
