# Standard environments.
ENVIRONMENT_BASE = "base"
ENVIRONMENT_DEVELOPMENT = "development"
ENVIRONMENT_CONTROL = "control"
ENVIRONMENT_TESTING = "testing"
ENVIRONMENT_STAGING = "staging"
ENVIRONMENT_LIVE = "live"

ENVIRONMENTS = {
    ENVIRONMENT_BASE: "The base environment represents the common dependencies and circumstances across all other "
                      "environments.",
    ENVIRONMENT_DEVELOPMENT: "The development environment is where code is developed and initially tested.",
    ENVIRONMENT_CONTROL: "The control environment is the local machine used for generating documentation and running "
                         "deployments.",
    ENVIRONMENT_TESTING: "The testing environment is where code is integrated and unit tests are performed.",
    ENVIRONMENT_STAGING: "The staging environment is where QA and test occurs.",
    ENVIRONMENT_LIVE: "The live environment is where the finished product lives or is hosted.",
}

EXIT_OK = 0
EXIT_FAILURE = 1
EXIT_UNKNOWN = 13

LOGGER_NAME = "deployable"

# TODO: Define standard roles.

SCOPE_ALL = "all"
SCOPE_DEPLOY = "deploy"
SCOPE_PROVISION = "provision"
SCOPE_TENANT = "tenant"

SCOPES = {
    SCOPE_ALL: "Commands or states representative of all scopes.",
    SCOPE_DEPLOY: "Commands or states that only apply when deploying a project",
    SCOPE_PROVISION: "Commands or states that only apply when provisioning servers for a project.",
    SCOPE_TENANT: "Commands or states that only apply when provisioning resources for a tenant.",
}
