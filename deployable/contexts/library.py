# Imports

import logging
import os
from ..constants import ENVIRONMENT_BASE, LOGGER_NAME as DEFAULT_LOGGER_NAME
from ..utils import filter_objects
from ..variables import factory as variable_factory, Variable

LOGGER_NAME = os.environ.get("PYTHON_DEPLOYABLE_LOGGER_NAME", DEFAULT_LOGGER_NAME)

logger = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "initialize_context",
    "Context",
)

# Functions


def initialize_context(environment, path="deployable", variables_from_command_line=None):
    """Initialize the global context.

    This processes the variables passed from the command line as well as the ``variables.ini`` file if one has been
    provided.

    :rtype: Context

    """
    context = Context()

    # Parse variables provided at the command line.
    if variables_from_command_line is not None:
        for spec in variables_from_command_line:
            name, value = spec.split(":")

            name = name.strip()
            value = value.strip()

            context.add(
                name,
                comment="%s initialized from command line." % name,
                value=value
            )

    # Load variables from the INI file.
    variables = list()
    variables_path = os.path.join(path, "_variables.ini")
    if os.path.exists(variables_path):
        logger.info("Load variables from %s" % variables_path)
        _variables = variable_factory(variables_path)

        variables = filter_objects(
            _variables,
            environments=[ENVIRONMENT_BASE, environment]
        )

    # Add variables to the context.
    logger.debug("Globals: %s" % variables)
    for v in variables:
        context.append(v)

    return context

# Classes


class Context(object):
    """A collection of variables that may be used throughout the deployment."""

    def __init__(self):
        self._attributes = dict()

    def __getattr__(self, item):
        if item in self._attributes:
            return self._attributes.get(item).value

        return None

    def add(self, name, value, comment=None, default=None, environments=None, roles=None, scope=None):
        """Add a variable to the context.

        :param name: The name of the variable.
        :type name: str

        :param value: The value of the variable, which may be ``None``.

        :param comment: A comment regarding the variable.
        :type comment: str

        :param default: The default value of the variable is applied when ``value`` is ``None``. Note that this means
                        the variable may *still* be initialized with a ``NoneType`` value, but this allows the value to
                        be set at runtime to a default when the actual value is unknown.

        :param environments: A list of environments in which the variable is intended for use.
        :type environments: list[str]

        :param roles: A list of roles to which the variable applies.
        :type roles: list[str]

        :param scope: The scope to which the variable applies.
        :type scope: str

        :rtype: Variable
        :returns: The variable instance that was just initialized.

        """
        v = Variable(
            name,
            comment=comment,
            default=default,
            environments=environments,
            roles=roles,
            scope=scope,
            value=value
        )
        self._attributes[name] = v

        return v

    def append(self, variable):
        """Add a variable instance to the context.

        :param variable: The variable instance.
        :type variable: Variable

        """
        self._attributes[variable.name] = variable

    @classmethod
    def from_ini(cls, path, obj=None):
        if obj is None:
            obj = cls()

        variables = variable_factory(path)
        for v in variables:
            obj.append(v)

        return obj

    def get(self, name, default=None):
        """Get the value of the named variable or it's default."""
        if self.has(name):
            return self._attributes.get(name).value

        return default

    def has(self, name):
        """Indicates whether the given attribute is defined and has a value other than ``None``.

        :param name: The attribute name.
        :type name: str

        :rtype: bool

        """
        if name in self._attributes:
            return self._attributes[name].value is not None

        return False

    # TODO: Context.load() has been replaced by variable factory(). Do we still need it?
    # def load(self, path, context=None):
    #     """Load variables from an INI file.
    #
    #     :param path: The path to the INI file.
    #     :type path: str
    #
    #     :param context: If given, the path will first be parsed as a Jinja2 template.
    #     :type context: dict
    #
    #     :rtype: bool
    #
    #     """
    #     if not os.path.exists(path):
    #         logger.warning("Context path does not exist: %s" % path)
    #         return False
    #
    #     variables = variable_factory(path, context=context)
    #     for v in variables:
    #         self.append(v)
    #
    #     return True

    def mapping(self):
        """Export context variables as a dictionary.

        :rtype: dict

        """
        d = dict()
        for name, variable in self._attributes.items():
            d[name] = variable.value

        return d

    def set(self, name, value):
        """Set (overwrite) a given variable with the given value.

        :param name: The name fo the variable.
        :type name: str
        :param value: The value to be set.

        """
        if not self.has(name):
            self._attributes[name] = Variable(name)

        self._attributes[name].value = value
