# Imports

from datetime import datetime
import logging
# from myninjas.utils import indent
import os
from ..constants import LOGGER_NAME as DEFAULT_LOGGER_NAME

LOGGER_NAME = os.environ.get("PYTHON_DEPLOYABLE_LOGGER_NAME", DEFAULT_LOGGER_NAME)

logger = logging.getLogger(LOGGER_NAME)

# Exports

# Classes


class Deploy(object):
    """Encapsulates all aspects of a given deployment."""

    def __init__(self, environment, path, release, user="deploy"):
        """Initialize the instance.

        :param environment: The deploy environment.
        :type environment: str

        :param path: The base path (root) of the deployment.
        :type path: str

        :param release: The release identifier.
        :type release: str

        :param user: The name of the user on the target hosts that is authorized to perform deployment step.
        :type user: str

        """
        self.end_dt = None
        self.environment = environment
        self.release = release
        self.root = path
        self.start_dt = None
        self.user = user

        self.current_path = os.path.join(path, "releases", "current")
        self.release_path = os.path.join(path, "releases", release)
        self.shared_path = os.path.join(path, "shared")

        # release_path = os.path.join(root, "releases", release)
        # current_path = os.path.join(root, "current")
        # shared_path = os.path.join(root, "shared")
        # backup_path = os.path.join(shared_path, "backups")
        # log_path = os.path.join(shared_path, "logs")
        # maint_path = os.path.join(shared_path, "maint")
        # tenant_path = os.path.join(shared_path, "tenants")

    def __repr__(self):
        return "<%s %s:%s>" % (self.__class__.__name__, self.environment, self.root)

    def end(self):
        """Mark the end of the deployment."""
        self.end_dt = datetime.now()

    @property
    def env(self):
        """Alias for ``environment``."""
        return self.environment

    def start(self):
        """Mark the beginning of the deployment."""
        self.start_dt = datetime.now()

    # def preview(self):
    #     """Get a preview of the actions to be taken during the deployment."""
    #     a = list()
    #     a.append("# Deploy")
    #     a.append("")
    #
    #     for host in self.hosts:
    #         a.append("## %s" % host.name)
    #         a.append("")
    #
    #         for role in host.roles:
    #             number = 1
    #             for step in role.steps:
    #                 a.append("%s) %s" % (number, step.comment))
    #                 a.append("")
    #
    #                 a.append(indent(step.preview()))
    #                 a.append("")
    #
    # @property
    # def root(self):
    #     """Get the path to deployment root on the server.
    #
    #     :rtype: str
    #
    #     """
    #     return "i/dont/know"
    #
    # def run(self):
    #     """Execute (run) the deployment across the requested hosts."""
    #     for host in self.hosts:
    #         for role in host.roles:
    #             for step in role.steps:
    #                 logger.info(step.command)
    #                 success = step.run()
    #
    #                 if success:
    #                     pass
    #                 elif step.stop:
    #                     logger.critical("Command failed with stop requested.")
    #                     break
    #                 else:
    #                     logger.warning("Command failed, but attempt to continue.")
