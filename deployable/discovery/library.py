# Imports

from ..commands import Command
from ..ssh import Client

# Exports

__all__ = (
    "Discovery",
)

# Classes


class Discovery(object):

    def __init__(self, host):
        self.host = host
        self.is_loaded = False
        self._attributes = dict()

        # Attributes set during load. Use of empty strings instead of None allows some class properties to work without
        # first checking for a NoneType value.
        self.hardware = ""
        self.hostname = ""
        self.ip = ""
        self.kernel = ""
        self.os = ""
        self.package_manager = ""
        self.platform = ""
        self.python_version = ""

    def get_distribution(self):
        """Get the name of the operating system distribution.

        :rtype: str

        """
        output = self.platform.lower()
        if "centos" in output:
            return "centos"
        elif "darwin" in output:
            return "darwin"
        elif "debian" in output:
            return "debian"
        elif "fedora" in output:
            return "fedora"
        elif "redhat" in output:
            return "redhat"
        elif "ubuntu" in output:
            return "ubuntu"
        elif "windows" in output:
            return "windows"
        else:
            return "unknown"

    def get_configuration_path(self):
        """Get the platform's specific path for persistent configuration files that should be writable by any
        application.

        :rtype: str | None

        """
        if self.is_linux:
            path = "~/.config"
        elif self.is_osx:
            path = "~/Library/Application Support"
        elif self.is_windows:
            path = "~/AppData/Roaming"
        else:
            path = None

        return path

    def get_temp_path(self):
        """Get the platform's specific path for temporary data that should be writable by any application.

        :rtype: str | None

        """
        if self.is_linux:
            path = "~/.cache"
        elif self.is_osx:
            path = "~/Library/Caches"
        elif self.is_windows:
            path = "~/AppData/Locals"
        else:
            path = None

        return path

    @property
    def is_linux(self):
        """Indicates this is a Linux operating system.

        :rtype: bool

        """
        return "linux" in self.platform.lower()

    @property
    def is_osx(self):
        """Indicates this is a Mac OS (Darwin) operating system.

        :rtype: bool

        """
        return "darwin" in self.platform.lower()

    @property
    def is_python2(self):
        """Indicates the current Python version is 2.

        :rtype: bool

        """
        return "2." in self.python_version

    @property
    def is_python3(self):
        """Indicates the current Python version is 3.

        :rtype: bool

        """
        return "3." in self.python_version

    @property
    def is_windows(self):
        """Indicates this is a Windows operating system.

        :rtype: bool

        """
        return "windows" in self.platform.lower()

    def load(self):
        client = Client(self.host)

        # Start by getting the platform.
        command = Command("python -m platform -c 'exec(print(platform.platform()))'")
        command.client = client
        if command.run():
            self.platform = command.output.strip()
        else:
            return False

        # TODO: Load more information about a Windows host.
        # If this is a Windows host, there is less to do.
        if self.is_windows:
            self.is_loaded = True
            return True

        commands = {
            'hardware': "uname -m",
            'hostname': "hostname",
            'ip': "ip -o -4 addr list eth0 | awk '{print $4}' | cut -d/ -f 1",
            'kernel': "uname -v",
            'os': "uname",
            'python_version': "python -V",
        }
        for attribute, statement in commands.items():
            command = Command(statement)
            command.client = client
            if command.run():
                setattr(self, attribute, command.output.strip())

        # https://www.tecmint.com/linux-package-managers/
        # unbuntu, debian, gentoo (portage), arch linux, redhat/fedora/cent, openSUSE
        # https://www.tecmint.com/apt-advanced-package-command-examples-in-ubuntu/
        # https://www.tecmint.com/dpkg-command-examples/
        # https://wiki.gentoo.org/wiki/Project:Portage
        # https://wiki.archlinux.org/index.php/Pacman
        # https://www.tecmint.com/20-linux-yum-yellowdog-updater-modified-commands-for-package-mangement/
        # https://www.tecmint.com/zypper-commands-to-manage-suse-linux-package-management/
        self._attributes['package_manager'] = None
        managers = ["apt-get", "dpkg", "emerge", "pacman", "yum", "zypper"]
        for manager in managers:
            command = Command("which %s" % manager)
            command.client = client

            if command.run():
                setattr(self, "package_manager", manager)
                break

        self.is_loaded = True
        return True
