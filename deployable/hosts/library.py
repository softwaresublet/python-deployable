# Imports

from configparser import ConfigParser
import logging
from myninjas.utils import smart_cast
import os
from ..constants import ENVIRONMENT_BASE, LOGGER_NAME as DEFAULT_LOGGER_NAME
from ..utils import expand_key_file

LOGGER_NAME = os.environ.get("PYTHON_DEPLOYABLE_LOGGER_NAME", DEFAULT_LOGGER_NAME)

logger = logging.getLogger(LOGGER_NAME)

# Exports

__all__ = (
    "factory",
    "Host",
)

# Functions


def factory(path, defaults=None):
    """Get host instances from a configuration file.

    :param path: The path to the configuration file.
    :type path: str

    :param defaults: The defaults passed to the configuration parser.
    :type defaults: dict

    :rtype: list[Host] | None

    """
    ini = ConfigParser(defaults=defaults)
    ini.read(path)

    hosts = list()
    for host_name in ini.sections():
        _kwargs = dict()

        for key, value in ini.items(host_name):
            # Environment is established by the name of the hosts INI file.
            # if key in ("env", "envs", "environments"):
            #     key = "environments"
            #     _value = list()
            #     for i in value.split(","):
            #         i = i.replace("[", "")
            #         i = i.replace("]", "")
            #         _value.append(i.strip())
            if key in ("role", "roles"):
                key = "roles"
                _value = list()
                for i in value.split(","):
                    i = i.replace("[", "")
                    i = i.replace("]", "")
                    _value.append(i.strip())
            else:
                _value = smart_cast(value)

            _kwargs[key] = _value

        hosts.append(Host(host_name, **_kwargs))

    return hosts

# Classes


class Host(object):
    """Represents a host to which something is deployed."""

    def __init__(self, name, **kwargs):
        """Initialize a host instance.

        :param name: The host name. For example: staging.example.com
        :type name: str

        :param environments: A list of environments this host supports. Default: ``["base"]``. See constants.
        :type environments: list[str]

        :param ip: The IP address of the host. See the ``address`` property.
        :type ip: str

        :param key_file: The SSH key to use for connecting to the host.
        :type key_file: str

        :param port: The SSH port number to use for connecting to the host. Default: ``22``.
        :type port: int

        :param roles: A list of roles this host provides. Default: ``["common"]``. See constants.
        :type roles: list[str]

        :param user: The SSH user name to use for connecting to the host. If omitted, the name will be derived from the
                     host ``name``. See ``guess_user_name()``.
        :type user: host.

        .. note::
            Any additional attributes are saved internally and may be accessed by name.

        """
        self.environments = kwargs.pop("environments", [ENVIRONMENT_BASE])
        self.ip = kwargs.pop("ip", None)
        self.name = name
        self.port = kwargs.pop("port", 22)
        self.roles = kwargs.pop("roles", ["common"])
        self.user = kwargs.pop("user", self.guess_user_name())

        key_file = kwargs.pop("key_file", self.guess_user_name())
        self.key_file = expand_key_file(key_file)

        # TODO: Add support for comment, title, tags, and grouping?
        self._attributes = kwargs

    def __getattr__(self, item):
        return self._attributes.get(item)

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self.name)

    @property
    def address(self):
        if self.ip is not None:
            return self.ip

        return self.name

    # def expand_key_file(self, path):
    #     if path is None:
    #         path = os.path.join("~/.ssh", self.guess_user_name())
    #
    #     if "/" not in path:
    #         path = os.path.join("~/.ssh", path)
    #
    #     return os.path.expanduser(path)

    # def get_command(self):
    #     """Get the SSH command used to connect to the host.
    #
    #     :rtype: str
    #
    #     """
    #     a = list()
    #     if self.key_file is not None:
    #         a.append("ssh -i %s" % self.key_file)
    #     else:
    #         a.append("ssh")
    #
    #     a.append("-p %s %s@%s" % (self.port, self.user, self.address))
    #
    #     return " ".join(a)

    def guess_user_name(self):
        """Guess the user name from the name of the host.

        :rtype: str

        """
        tokens = self.name.split(".")

        # staging.example.com becomes example.com, but example.com becomes com, which is probably not desirable.
        if len(tokens) == 2:
            return "_".join(tokens)

        tokens.pop(0)
        return "_".join(tokens)
