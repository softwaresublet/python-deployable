from myninjas.logging import LoggingHelper
import os

LOGGER_NAME = os.environ.get("DEPLOYABLE_LOGGER_NAME", "deployable")
LOGGER_FILE = os.environ.get("DEPLOYABLE_LOG_FILE", None)

log = LoggingHelper(colorize=True, name=LOGGER_NAME, path=LOGGER_FILE, success=True)
logger = log.setup()

# noinspection PyUnresolvedReferences
logger.success('Initialized "%s" logger.' % LOGGER_NAME)
