# Imports

import logging
import os
from deployable.steps.factory import Config

logger = logging.getLogger("deployable")

# Exports

__all__ = (
    "factory",
    "Role",
)

# Constants

DEFAULT_PATH = os.path.join("deployable", "deploy")

# Functions


def factory(name, context=None, path=DEFAULT_PATH):
    """Get a role instance from a configuration file.

    :param name: The name of the role.
    :type name: str

    :param context: The context to be passed to the command factory.
    :type context: Context

    :param path: The path to deployment files.
    :type path: str

    :rtype: Role | None

    """
    # Get the path to the steps.cfg file.
    _path = os.path.join(path, "roles", name, "steps.cfg")
    if not os.path.exists(_path):
        logger.warning("The *%s* role does not exist: %s" % (name, _path))
        return None

    # Load the config. A bad path is currently the only reason Config.load() returns False and the path is checked
    # above.
    config = Config(_path, context=context)
    config.load()

    # Initialize the role.
    return Role(name, os.path.dirname(_path), steps=config.get_steps())

# Classes


class Role(object):
    """A role is a collection of steps and states applied to a host."""

    def __init__(self, name, path, steps=None):
        """Initialize the role.

        :param name: The name of the role.
        :type name: str

        :param path: The path to the role's directory.
        :type path: str

        """
        self.name = name
        self.path = path
        self.steps = steps or list()

    def __iter__(self):
        return iter(self.steps)

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self.name)

    # def get_path(self, path):
    #     base = os.path.dirname(self.path)
    #     return os.path.join(base, path)
    #
    # def has_file(self, path):
    #     return os.path.exists(self.get_path(path))
