# Imports

from myninjas.utils import write_file
import os
from paramiko import AutoAddPolicy, SSHClient

# Exports

__all__ = (
    "Client",
)

# Constants

KNOWN_HOSTS_FILE = os.path.join("deployable", ".known_hosts")

# Classes


class Client(SSHClient):
    """A host-aware SSH client."""

    def __init__(self, host, known_hosts=KNOWN_HOSTS_FILE):
        """Initialize the client.

        :param host: The host instance to use for the SSH connection.
        :type host: deployable.hosts.Host

        """
        self.host = host
        self.known_hosts = known_hosts

        if not os.path.exists(known_hosts):
            write_file(known_hosts, make_directories=True)

        super().__init__()

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self.host.name)

    def connect(self, **kwargs):
        """Provide host values to the SSH connection."""
        if "key_filename" not in kwargs:
            kwargs['key_filename'] = self.host.key_file

        if "port" not in kwargs:
            kwargs['port'] = self.host.port

        if "username" not in kwargs:
            kwargs['username'] = self.host.user

        self.load_host_keys(self.known_hosts)
        self.set_missing_host_key_policy(AutoAddPolicy())

        super().connect(self.host.address, **kwargs)

    def upload(self, from_path, to_path):
        """Upload a file to the host.

        :param from_path: The local file path.
        :type from_path: str

        :param to_path: The remote  file path.
        :type to_path: str

        :rtype: bool

        """
        transport = self.get_transport()
        if transport is None:
            self.connect()

        sftp = self.open_sftp()
        sftp.put(from_path, to_path)

        return True
