"""
Apache
......

Classes for working with commands related to the Apache web server.

"""

# Imports

from deployable.commands import Command
from ..base import Step

# Exports

__all__ = (
    "MAPPING",
    "ConfigTest",
    "DisableModule",
    "DisableSite",
    "EnableModule",
    "EnableSite",
)

# Classes


class ConfigTest(Step):
    """Test the Apache configuration."""

    def __init__(self, **kwargs):
        name = kwargs.get("comment", "test apache configuration")
        super().__init__(name, **kwargs)

    def get_commands(self):
        return [Command("apachectl configtest", **self._command_kwargs)]


class DisableModule(Step):
    """Disable an Apache module."""

    def __init__(self, module, **kwargs):
        self.module = module

        name = kwargs.get("comment", "disable %s module" % module)
        super().__init__(name, **kwargs)

    def get_commands(self):
        return [Command("a2dismod %s" % self.module, **self._command_kwargs)]


class DisableSite(Step):
    """Disable website."""

    def __init__(self, domain, **kwargs):
        self.domain = domain

        name = kwargs.get("comment", "disable %s" % domain)
        super().__init__(name, **kwargs)

    def get_commands(self):
        if ".conf" in self.domain:
            domain = self.domain
        else:
            domain = self.domain + ".conf"

        return [Command("a2dissite %s" % domain, **self._command_kwargs)]


class EnableModule(Step):
    """Enable an apache module."""

    def __init__(self, module, **kwargs):
        self.module = module

        name = kwargs.get("comment", "enable %s module" % module)
        super().__init__(name, **kwargs)

    def get_commands(self):
        return [Command("a2enmod %s" % self.module, **self._command_kwargs)]


class EnableSite(Step):
    """Enable a website."""

    def __init__(self, domain, **kwargs):
        self.domain = domain

        name = kwargs.get("comment", "enable %s" % domain)
        super().__init__(name, **kwargs)

    def get_commands(self):
        if ".conf" in self.domain:
            domain = self.domain
        else:
            domain = self.domain + ".conf"

        return [Command("a2ensite %s" % domain, **self._command_kwargs)]


MAPPING = {
    'apache.config': ConfigTest,
    'apache.configtest': ConfigTest,
    'apache.disable_mod': DisableModule,
    'apache.disable_site': DisableSite,
    'apache.enable_mod': EnableModule,
    'apache.enable_module': EnableModule,
    'apache.enable_site': EnableSite,
    'apache.test': ConfigTest,
}
