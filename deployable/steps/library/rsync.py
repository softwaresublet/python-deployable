"""
Rsync
.....

Classes for working with rsync.

"""
# Imports

import os
from deployable.commands import Command
from ..base import Step

# Exports

__all__ = (
    "MAPPING",
    "Rsync",
    "RsyncCommand",
)

# Classes


class Rsync(Step):
    """Use rsync to copy files and directories."""

    def __init__(self, source, target, delete=False, host=None, key_file=None, links=True, port=22, recursive=True,
                 user=None, **kwargs):
        """Use rsync to copy files and directories to another location.

        :param source: The source directory.
        :type source: str

        :param target: The target directory.
        :type target: str

        :param delete: Indicates files that exist in target but not in source should be deleted.
        :type delete: bool

        :param host: The host name or IP address when syncing to a remote directory. Note that using this option will
                     require the ``key_file`` and ``user`` parameters.
        :type host: str

        :param key_file: The name or path of the key file to use for remote operations.
        :type key_file: str

        :param links: Indicates symlinks should also be copied.
        :type links: bool

        :param port: The SSH port number used for remote operations.
        :type port: int

        :param recursive: Indicates sub-directories should be copied.
        :type recursive: bool

        :param user: The user account for remote operations.
        :type user: str

        Additional keyword arguments are passed to the command instances.

        """
        self.copy_links = links
        self.delete = delete
        self.host = host
        self.key_file = key_file
        self.port = port
        self.recursive = recursive
        self.source = source
        self.target = target
        self.user = user

        # Files must always be synced from the machine upon which the command is running to a remote machine.
        # kwargs['local'] = True
        # kwargs['sudo'] = False

        name = kwargs.get("comment", "rsync %s" % source)
        super().__init__(name, **kwargs)

    def get_commands(self):
        # rsync -e "ssh -i $(SSH_KEY) -p $(SSH_PORT)" -P -rvzc --delete
        # $(OUTPUTH_PATH) $(SSH_USER)@$(SSH_HOST):$(UPLOAD_PATH) --cvs-exclude;
        return [RsyncCommand(
            self.source,
            self.target,
            delete=self.delete,
            host=self.host,
            key_file=self.key_file,
            links=self.copy_links,
            port=self.port,
            recursive=self.recursive,
            user=self.user,
            **self._command_kwargs
        )]


class RsyncCommand(Command):
    """A special command which accounts for the different between local and remote synchronization."""

    def __init__(self, source, target, delete=False, host=None, key_file=None, links=True, port=22, recursive=True,
                 user=None, **kwargs):
        """Use rsync to copy files and directories to another location.

        :param source: The source directory.
        :type source: str

        :param target: The target directory.
        :type target: str

        :param delete: Indicates files that exist in target but not in source should be deleted.
        :type delete: bool

        :param host: The host name or IP address when syncing to a remote directory. Note that using this option will
                     require the ``key_file`` and ``user`` parameters.
        :type host: str

        :param key_file: The name or path of the key file to use for remote operations.
        :type key_file: str

        :param links: Indicates symlinks should also be copied.
        :type links: bool

        :param port: The SSH port number used for remote operations.
        :type port: int

        :param recursive: Indicates sub-directories should be copied.
        :type recursive: bool

        :param user: The user account for remote operations.
        :type user: str

        """
        self.copy_links = links
        self.delete = delete
        self.host = host
        self.key_file = key_file
        self.port = port
        self.recursive = recursive
        self.source = source
        self.target = target
        self.user = user

        super().__init__("# sync files and directories", **kwargs)

    def get_statement(self, cwd=False):
        if self.is_local:
            return self._get_local_statement(cwd=cwd)

        return self._get_remote_statement()

    def _get_local_statement(self, cwd=False):
        statement = list()

        if self.cwd and cwd:
            statement.append("(cd %s &&" % self.cwd)

        statement.append('rsync --checksum --compress')

        if self.copy_links:
            statement.append("--copy-links")

        statement.append("--cvs-exclude")

        if self.delete:
            statement.append("--delete")

        # --partial and --progress
        statement.append("-P")

        if self.recursive:
            statement.append("--recursive")

        statement.append(self.source)
        statement.append(self.target)

        if self.cwd and cwd:
            return " ".join(statement) + ")"

        return " ".join(statement)

    def _get_remote_statement(self):
        """Get the statement for syncing files to a remote host.

        :rtype: str

        - ``host``, ``key_file``, and ``user`` are guessed if not provided.
        - ``local`` (``is_local``) and ``sudo`` parameters are ignored.
        - ``cwd`` is also ignored, requiring ``source`` to be a full path.

        """
        if self.host is not None:
            host = self.host
        else:
            host = os.path.basename(self.source).replace("_", ".")

        if self.key_file is not None:
            key_file = self.key_file
        else:
            key_file = os.path.join("~/.ssh", os.path.basename(self.source))

        key_file = os.path.expanduser(key_file)

        if self.user is None:
            user = os.path.basename(self.source)
        else:
            user = self.user

        statement = list()
        statement.append('rsync -e "ssh -i %s -p %s"' % (key_file, self.port))
        statement.append("--checksum")
        statement.append("--compress")

        if self.copy_links:
            statement.append("--copy-links")

        statement.append("--cvs-exclude")

        if self.delete:
            statement.append("--delete")

        # --partial and --progress
        statement.append("-P")

        if self.recursive:
            statement.append("--recursive")

        statement.append(self.source)

        statement.append("%s@%s:%s" % (user, host, self.target))

        return " ".join(statement)


MAPPING = {
    'rsync': Rsync,
    'sync': Rsync,
}
