"""
Tarball
.......

Classes for working with archiving and extracting TGZ files.

"""
# Imports

import os
from deployable.commands import Command
from ..base import Step

# Exports

__all__ = (
    "MAPPING",
    "Archive",
    "Extract",
)

# Classes


class Archive(Step):
    """Create an archive of a path."""

    def __init__(self, path, absolute=False, exclude=None, file_name="archive.tgz", strip=0, to_path=".",
                 view=False, **kwargs):
        """Create an archive file.

        :param path: The path that should be archived.
        :type path: str

        :param absolute: By default, the leading slash is stripped from each path. Set to ``True`` to preserve the
                         absolute path.
        :type absolute: bool

        :param exclude: A pattern to be excluded from the archive.
        :type exclude: str

        :param file_name: The name of the archive file.
        :type file_name: str

        :param strip: Remove the specified number of leading elements from the path. Paths with fewer elements will be
                      silently skipped.
        :type strip: int

        :param to_path: Where the archive should be created. This should *not* include the file name.
        :type to_path: str

        :param view: View the output of the command as it happens.
        :type view: bool

        """
        self.absolute = absolute
        self.exclude = exclude
        self.file_name = file_name
        self.path = path
        self.strip = strip
        self.to_path = to_path
        self.view = view

        name = kwargs.get("comment", "archive %s" % path)
        super().__init__(name, **kwargs)

    def get_commands(self):
        statement = ["tar"]

        switches = ["-cz"]

        if self.absolute:
            switches.append("P")
        
        if self.view:
            switches.append("v")
            
        statement.append("".join(switches))

        if self.exclude:
            statement.append("--exclude %s" % self.exclude)

        if self.strip:
            statement.append("--strip-components %s" % self.strip)

        to_path = os.path.join(self.to_path, self.file_name)
        statement.append('-f %s %s' % (to_path, self.path))

        return [Command(statement, **self._command_kwargs)]


class Extract(Step):
    """Extract an archive."""

    def __init__(self, path, absolute=False, exclude=None, strip=0, to_path=".",
                 view=False, **kwargs):
        """Extract an archive file.

        :param path: The path to the archive file.
        :type path: str

        :param absolute: By default, the leading slash is stripped from each path. Set to ``True`` to preserve the
                         absolute path.
        :type absolute: bool

        :param exclude: A pattern to be excluded from the archive.
        :type exclude: str

        :param strip: Remove the specified number of leading elements from the path. Paths with fewer elements will be
                      silently skipped.
        :type strip: int

        :param to_path: Where the archive should be extracted.
        :type to_path: str

        :param view: View the output of the command as it happens.
        :type view: bool

        """
        self.absolute = absolute
        self.exclude = exclude
        self.path = path
        self.strip = strip
        self.to_path = to_path
        self.view = view

        name = kwargs.get("comment", "extract %s" % path)
        super().__init__(name, **kwargs)

    def get_commands(self):
        statement = ["tar"]

        switches = ["-xz"]

        if self.absolute:
            switches.append("P")

        if self.view:
            switches.append("v")

        statement.append("".join(switches))

        if self.exclude:
            statement.append("--exclude %s" % self.exclude)

        if self.strip:
            statement.append("--strip-components %s" % self.strip)

        statement.append('-f %s %s' % (self.path, self.to_path))

        return [Command(statement, **self._command_kwargs)]


MAPPING = {
    'archive': Archive,
    'extract': Extract,
}
