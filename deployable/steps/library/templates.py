"""
Templates
.........

Classes for creating files from pre-defined Jinja2 templates.

"""
# Imports

# noinspection PyPackageRequirements
from jinja2 import TemplateNotFound
import logging
from myninjas.utils import parse_jinja_template, write_file
import os
from deployable.commands import Command
from deployable.constants import ENVIRONMENT_BASE, SCOPE_DEPLOY
from ..base import Step

logger = logging.getLogger("deployable")

# Exports

__all__ = (
    "MAPPING",
    "Template",
    "TemplateCommand",
    "TemplateNotFound",
)

# Classes


class Template(Step):
    """Create a file from a template."""

    def __init__(self, source, target, backup=".b", **kwargs):

        name = kwargs.get("comment", "create file from %s template" % os.path.basename(source))

        _kwargs = {
            'comment': kwargs.pop("comment", None),
            'cwd': kwargs.pop("cwd", None),
            'environments': kwargs.pop("environments", [ENVIRONMENT_BASE]),
            'prefix': kwargs.pop("prefix", None),
            'scope': kwargs.pop("scope", SCOPE_DEPLOY),
            'shell': kwargs.pop("shell", "/bin/bash"),
            'stop': kwargs.pop("stop", False),
            'sudo': kwargs.pop("sudo", True),
            'tags': kwargs.pop("tags", list()),
        }

        self.backup = backup
        self.context = kwargs.pop("context", dict())
        self.locations = kwargs.pop("locations", list())
        self.source = source
        self.target = target

        # Remaining kwargs are added to the context.
        self.context.update(kwargs)

        super().__init__(name, **_kwargs)

        if _kwargs['cwd'] is not None:
            logger.warning("Template commands do not support path switching.")

    def get_commands(self):
        # Start collecting the commands.
        commands = list()

        # Create the command used to back up the existing file.
        statement = "mv %s %s%s" % (self.target, self.target, self.backup)
        commands.append(Command(statement, **self._command_kwargs))

        # Create the command used to generate the template. See TemplateCommand.
        command = TemplateCommand(self.source, self.target, context=self.context, locations=self.locations,
                                  **self._command_kwargs)
        commands.append(command)

        return commands


class TemplateCommand(Command):
    """A special command for parsing a template and uploading the result to a target file via FTP."""

    def __init__(self, source, target, context=None, locations=None, **kwargs):
        self.context = context
        self.locations = locations
        self.source = source
        self.target = target

        super().__init__("# create file from template", **kwargs)

    def get_content(self):
        """Get the content of the target file by parsing context into source.

        :rtype: str | None

        """
        # Generate output.
        template = self.get_template()

        try:
            return parse_jinja_template(template, self.context)
        except TemplateNotFound:
            logger.error("Template not found: %s" % template)
            return None

    def get_statement(self, cwd=False):
        """The command statement is *only* use when the command is processed locally."""

        # noinspection PyListCreation
        statement = ["cat > %s << EOF" % self.target]
        statement.append(self.get_content() or "")
        statement.append("EOF")

        return "\n".join(statement)

    def get_template(self):
        """Get the full path to the template.

        :rtype: str

        """
        source = self.source
        for location in self.locations:
            _source = os.path.join(location, self.source)
            if os.path.exists(_source):
                return _source

        return source

    def _remote(self):
        """Override remote execution to use SFTP."""

        # Write the output to a temporary location on the local machine.
        content = self.get_content()
        if content is None:
            logger.warning("Can't upload an empty file to the remote machine.")
            return False

        output_file = os.path.join("deployable", ".tmp", os.path.basename(self.target))
        logger.debug("Write template output to temporary file: %s" % output_file)
        write_file(output_file, content=content, make_directories=True)

        # Upload the output file to a temporary location on the remote machine.
        remote_output_file = os.path.join("/tmp", os.path.basename(self.target))
        logger.debug("Upload target file to temporary location: %s" % remote_output_file)
        self.client.upload(output_file, remote_output_file)

        # Remove the temporary file.
        logger.debug("Remove the temporary file: %s" % output_file)
        os.remove(output_file)

        # Move the file from the remote and temporary location to the final location.
        logger.debug("Move target file to final location: %s" % self.target)
        command = Command("mv %s %s" % (remote_output_file, self.target), sudo=self.sudo)
        command.client = self.client
        return command.run()


MAPPING = {
    'template': Template,
    'tpl': Template,
}
