# Imports

import os
import random
import string
from .constants import SCOPE_ALL

# Exports

__all__ = (
    "any_list_item",
    "expand_key_file",
    "filter_objects",
    "generate_random_password",
)

# Functions


def any_list_item(a, b):
    """Determine whether any item in ``a`` also exists in ``b``.

    :param a: The first list to be compared.
    :type a: list

    :param b: The second list to be compared.
    :type b: list

    :rtype: bool

    """
    for i in a:
        for j in b:
            if i == j:
                return True

    return False


def expand_key_file(name):
    """Find the named key file.

    :param name: The name or path of the file.
    :type name: str

    :rtype: str | None
    :returns: The full path to the key file, including user expansion.

    """
    locations = [
        name,
        os.path.join("~/.ssh", name),
        os.path.join("deployable", "keys", name),
    ]
    for path in locations:
        _expanded_path = os.path.expanduser(path)
        if os.path.exists(_expanded_path):
            return _expanded_path

    return None


def filter_objects(objects, environments=None, scope=None, tags=None):
    filtered = list()

    # print("object, object environments, environments, any_list_item")

    for o in objects:

        # print(o, o.environments, environments, any_list_item(environments, o.environments))

        # Apply environment filter.
        if environments is not None:
            if not any_list_item(environments, o.environments):
                continue

        # Apply scope filter.
        if scope is not None:
            if o.scope not in [None, SCOPE_ALL, scope]:
                continue

        # Apply tag filter.
        if tags is not None:
            if not any_list_item(tags, o.tags):
                continue

        # The object has passed the tests above.
        filtered.append(o)

    return filtered


def generate_random_password(length=10):
    """Create a random password.

    :param length: The length of the password.
    :type length: int

    :rtype: str

    """
    good_characters = string.ascii_letters + string.digits

    password = ""
    i = 1
    while i <= length:
        random_number = random.randint(1, 126)
        character = chr(random_number)
        if character not in good_characters:
            continue

        password += character
        i += 1

    return password
