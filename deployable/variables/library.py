# Imports

from configparser import ConfigParser
from myninjas.utils import is_bool, is_integer, parse_jinja_template, smart_cast, to_bool
from ..constants import ENVIRONMENT_BASE, SCOPE_DEPLOY

# Exports

__all__ = (
    "factory",
    "Variable",
)

# Functions


def factory(path, context=None, defaults=None):
    """Load variables from an INI file.

    :param path: The path to the file.
    :type path: str

    :param context: If given, the path will first be parsed as a Jinja2 template.
    :type context: dict

    :param defaults: The defaults passed to the configuration parser.
    :type defaults: dict

    :rtype: list[Variable]

    """
    if context is not None:
        content = parse_jinja_template(path, context)

        ini = ConfigParser()
        ini.read_string(content)
    else:
        ini = ConfigParser(defaults=defaults)
        ini.read(path)

    variables = list()
    for section in ini.sections():
        _kwargs = dict()
        for key, value in ini.items(section):
            if key in ("env", "envs", "environments"):
                key = "environments"
                _value = list()
                for i in value.split(","):
                    _value.append(i.strip())
            elif key in ("role", "roles"):
                key = "roles"
                _value = list()
                for i in value.split(","):
                    _value.append(i.strip())
            else:
                _value = smart_cast(value)

            _kwargs[key] = _value

        variables.append(Variable(section, **_kwargs))

    return variables

# Classes


class Variable(object):
    """A placeholder class for deployment variables."""

    def __init__(self, name, **kwargs):
        """Initialize the variable.

        :param name: The variable name.
        :type name: str

        :param environments: A list of environments to which this variable applies. Default: ``["base"]``. See
                            constants.
        :type environments: list[str]

        :param roles: A list of roles this host provides. Default: ``["common"]``. See constants.
        :type roles: list[str]

        :param user: The SSH user name to use for connecting to the host. If omitted, the name will be derived from the
                     host ``name``. See ``guess_user_name()``.
        :type user: host.

        .. note::
            Any additional attributes are saved internally and may be accessed by name.
        """
        self.comment = kwargs.pop("comment", None)
        self.default = kwargs.pop("default", None)
        self.environments = kwargs.pop("environments", [ENVIRONMENT_BASE])
        self.name = name
        self.roles = kwargs.pop("roles", ["common"])
        self.scope = kwargs.pop("scope", SCOPE_DEPLOY)

        value = kwargs.pop("value", self.default)
        if value is not None:
            if is_integer(value, cast=True):
                value = int(value)
            elif is_bool(value):
                value = to_bool(value)
            else:
                pass

        self.value = value

    def __eq__(self, other):
        return other.value == self.value

    def __repr__(self):
        return "<%s.%s %s:%s>" % (self.__class__.__name__, self.scope, self.name, self.value)

    def __str__(self):
        if self.value is None:
            return ""

        return str(self.value)

    @property
    def type(self):
        """Get the Python data type of the variable."""
        return type(self.value)
