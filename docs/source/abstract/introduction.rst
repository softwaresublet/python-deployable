************
Introduction
************

The value of a given application may not be realized until it is available for use.

Provisioning resources and deploying an application represents a non-trivial collection of steps. The value of a
repeatable release process should be well-understood. Without this, each release poses a significant risk to the
operation of the application which may impact users in a negative manner.

The purpose of the process is therefore two-fold:

1. Ease the administrative burden for releasing software.
2. Reduce the risk of releasing software.

Environments
============

An *Environment* represents an operational state of an application.

1. development
2. testing
3. staging
4. live

To these we may also add:

- base
- control

Scopes
======

A *Scope* represents an overall collection of steps required to produce a given outcome.

1. provision: The provision scope includes those steps required to prepare a target environment for a deployment.
2. deploy: The deploy scope includes those steps required to create a functional system.
3. tenant: For multi-tenant applications, the tenant scope includes those steps required to set up a new tenant.

Release
=======

A *Release* is a collection of changes to be deployed to a target environment.

It is possible for a release to span all three Scopes.

- provision: A new server may be required or a new or updated package may need to be installed on the servers.
- deploy: New features or bug fixes may need to be pushed to the application server.
- tenant: Changes to database schema may need to be applied to tenant databases.

A number of common steps may be performed with each release. For example:

1. Push the latest source code to the target environment.
2. Apply database migrations.
3. Reload services.

It is also possible that a release includes non-standard changes. For example:

- Run SQL to update records that have changed due to a database migration.
