********
Releases
********

A *Release* is a collection of changes to be deployed to a target environment.
