*********
Deploying
*********

A *deployment* applies the necessary changes to a target environment (host) in order for an application or site to
function.



Provision the host or hosts to which releases will be deployed.

Identify the host or hosts to which the release will be applied.

Prepare each host to do its part in serving the application.

.. Push source code to the host (or hosts) that run application code.


.. code-block:: bash

    cd $PROJECT_HOME/project_name;
    deployable deploy staging;
