Deployable
==========

Contents:

.. toctree::
    :maxdepth: 3

    Abstract <abstract/index>
    Usage <usage/index>
    Reference <reference>
    Tests <tests>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
