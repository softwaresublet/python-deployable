adduser
^^^^^^^

.. code-block:: cfg

    add a user account:
        adduser name
        groups None
        home None

apache.config
^^^^^^^^^^^^^

.. code-block:: cfg

    test the apache configuration:
        apache.config 

apache.configtest
^^^^^^^^^^^^^^^^^

.. code-block:: cfg

    test the apache configuration:
        apache.configtest 

apache.disable_mod
^^^^^^^^^^^^^^^^^^

.. code-block:: cfg

    disable an apache module:
        apache.disable_mod module

apache.disable_site
^^^^^^^^^^^^^^^^^^^

.. code-block:: cfg

    disable website:
        apache.disable_site domain

apache.enable_mod
^^^^^^^^^^^^^^^^^

.. code-block:: cfg

    enable an apache module:
        apache.enable_mod module

apache.enable_module
^^^^^^^^^^^^^^^^^^^^

.. code-block:: cfg

    enable an apache module:
        apache.enable_module module

apache.enable_site
^^^^^^^^^^^^^^^^^^

.. code-block:: cfg

    enable a website:
        apache.enable_site domain

apache.test
^^^^^^^^^^^

.. code-block:: cfg

    test the apache configuration:
        apache.test 

append
^^^^^^

.. code-block:: cfg

    append to a file:
        append path
        content None

apt
^^^

.. code-block:: cfg

    install a package using apt-get:
        apt package

archive
^^^^^^^

.. code-block:: cfg

    create an archive of a path:
        archive path
        absolute False
        exclude None
        file_name archive.tgz
        strip 0
        to_path .
        view False

certbot
^^^^^^^

.. code-block:: cfg

    generate an ssl certicate using let's encrypt:
        certbot domain_name
        email None
        webroot None

copy
^^^^

.. code-block:: cfg

    copy a file or directory:
        copy from_path to_path
        recursive False

cp
^^

.. code-block:: cfg

    copy a file or directory:
        cp from_path to_path
        recursive False

django
^^^^^^

.. code-block:: cfg

    run a django management command (additional options are interpreted as options for the command):
        django management_command

echo
^^^^

.. code-block:: cfg

    echo a message to the console:
        echo output

extract
^^^^^^^

.. code-block:: cfg

    extract an archive:
        extract path
        absolute False
        exclude None
        strip 0
        to_path .
        view False

feedback
^^^^^^^^

.. code-block:: cfg

    echo a message to the console:
        feedback output

link
^^^^

.. code-block:: cfg

    create a symlink:
        link source
        force False
        target None

makedir
^^^^^^^

.. code-block:: cfg

    create a directory:
        makedir path
        mode None
        recursive True

message
^^^^^^^

.. code-block:: cfg

    echo a message to the console:
        message output

mkdir
^^^^^

.. code-block:: cfg

    create a directory:
        mkdir path
        mode None
        recursive True

msg
^^^

.. code-block:: cfg

    echo a message to the console:
        msg output

perm
^^^^

.. code-block:: cfg

    set permissions on a file or directory:
        perm path
        group None
        mode None
        owner None
        recursive False

permissions
^^^^^^^^^^^

.. code-block:: cfg

    set permissions on a file or directory:
        permissions path
        group None
        mode None
        owner None
        recursive False

perms
^^^^^

.. code-block:: cfg

    set permissions on a file or directory:
        perms path
        group None
        mode None
        owner None
        recursive False

pg.client
^^^^^^^^^

.. code-block:: cfg

    run a postgres sql statement:
        pg.client sql
        database template1
        password None
        host localhost
        user postgres

pg.createdatabase
^^^^^^^^^^^^^^^^^

.. code-block:: cfg

    create a postgres database:
        pg.createdatabase name
        admin_pass None
        admin_user postgres
        host localhost
        owner None

pg.createdb
^^^^^^^^^^^

.. code-block:: cfg

    create a postgres database:
        pg.createdb name
        admin_pass None
        admin_user postgres
        host localhost
        owner None

pg.createuser
^^^^^^^^^^^^^

.. code-block:: cfg

    create a postgres user (role):
        pg.createuser name
        admin_pass None
        admin_user postgres
        host localhost
        password None

pg.database
^^^^^^^^^^^

.. code-block:: cfg

    create a postgres database:
        pg.database name
        admin_pass None
        admin_user postgres
        host localhost
        owner None

pg.databaseexists
^^^^^^^^^^^^^^^^^

.. code-block:: cfg

    determine if a postgres database exists:
        pg.databaseexists name
        admin_pass None
        admin_user postgres
        host localhost

pg.db
^^^^^

.. code-block:: cfg

    create a postgres database:
        pg.db name
        admin_pass None
        admin_user postgres
        host localhost
        owner None

pg.dbexists
^^^^^^^^^^^

.. code-block:: cfg

    determine if a postgres database exists:
        pg.dbexists name
        admin_pass None
        admin_user postgres
        host localhost

pg.dropdatabase
^^^^^^^^^^^^^^^

.. code-block:: cfg

    drop (remove) a postgres database:
        pg.dropdatabase name
        admin_pass None
        admin_user postgres
        host localhost

pg.dropdb
^^^^^^^^^

.. code-block:: cfg

    drop (remove) a postgres database:
        pg.dropdb name
        admin_pass None
        admin_user postgres
        host localhost

pg.dropuser
^^^^^^^^^^^

.. code-block:: cfg

    drop (remove) a postgres user (role):
        pg.dropuser name
        admin_pass None
        admin_user postgres
        host localhost

pg.dump
^^^^^^^

.. code-block:: cfg

    dump (export) a postgres database:
        pg.dump name
        admin_pass None
        admin_user postgres
        host localhost

pg.dumpdb
^^^^^^^^^

.. code-block:: cfg

    dump (export) a postgres database:
        pg.dumpdb name
        admin_pass None
        admin_user postgres
        host localhost

pg.export
^^^^^^^^^

.. code-block:: cfg

    dump (export) a postgres database:
        pg.export name
        admin_pass None
        admin_user postgres
        host localhost

pg.user
^^^^^^^

.. code-block:: cfg

    create a postgres user (role):
        pg.user name
        admin_pass None
        admin_user postgres
        host localhost
        password None

pip
^^^

.. code-block:: cfg

    install a python package using pip:
        pip package

psql
^^^^

.. code-block:: cfg

    run a postgres sql statement:
        psql sql
        database template1
        password None
        host localhost
        user postgres

reload
^^^^^^

.. code-block:: cfg

    reload a service:
        reload service

remove
^^^^^^

.. code-block:: cfg

    remove a file or directory:
        remove path
        force False
        recursive False

replace
^^^^^^^

.. code-block:: cfg

    replace text in a file:
        replace path
        backup .b
        delimiter /
        find None
        replace None

restart
^^^^^^^

.. code-block:: cfg

    restart a service:
        restart service

rm
^^

.. code-block:: cfg

    remove a file or directory:
        rm path
        force False
        recursive False

rsync
^^^^^

.. code-block:: cfg

    use rsync to copy files and directories:
        rsync source target
        delete False
        host None
        key_file None
        links True
        port 22
        recursive True
        user None

run
^^^

.. code-block:: cfg

    execute a command:
        run statement

sed
^^^

.. code-block:: cfg

    replace text in a file:
        sed path
        backup .b
        delimiter /
        find None
        replace None

start
^^^^^

.. code-block:: cfg

    start a service:
        start service

stop
^^^^

.. code-block:: cfg

    stop a service:
        stop service

symlink
^^^^^^^

.. code-block:: cfg

    create a symlink:
        symlink source
        force False
        target None

sync
^^^^

.. code-block:: cfg

    use rsync to copy files and directories:
        sync source target
        delete False
        host None
        key_file None
        links True
        port 22
        recursive True
        user None

template
^^^^^^^^

.. code-block:: cfg

    create a file from a template:
        template source target
        backup .b

touch
^^^^^

.. code-block:: cfg

    touch a file or directory:
        touch path

tpl
^^^

.. code-block:: cfg

    create a file from a template:
        tpl source target
        backup .b

virtualenv
^^^^^^^^^^

.. code-block:: cfg

    create a python virtual environment:
        virtualenv name

yum
^^^

.. code-block:: cfg

    install a package using yum:
        yum package

