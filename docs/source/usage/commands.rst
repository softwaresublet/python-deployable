********
Commands
********

The commands library encapsulates shell commands in an object-oriented fashion, making it easier to assemble, preview, 
and run shell commands, and obtain the results.

Using a Command
---------------

The shell library's Command provides a convenient, object-oriented representation of command line execution.

.. code-block:: python

    from deployable.commands import Command

    # A simple command.
    command = Command("ls -ls")

    # print(command.preview())

    if command.run():
        print(command.output)
        print("Your listing is above.")

    # Create /tmp/tmp.txt file.
    command = Command("touch tmp.txt", path="/tmp")
    command.run()

    # Using a prefix.
    command = Command("pg_dumpall -U postgres", prefix='export PGPASSWORD="mypassword"')
    command.run()

Quick and Easy Aborts
---------------------

It is convenient to issue a message at the same time as calling ``sys.exit()``.

.. code-block:: python

    from deployable.commands import abort

    # doing something important ...
    # still doing something important ...
    # oh, snap! there's no way to recover from this, I guess I'll have to ...
    abort("I can't go on.")

The Commands Library
--------------------

My Ninjas ships with a library of classes that help pre-define a number of command line activities. These may be used
programmatically, assembled using :py:class:`deployable.commands.scripts.Script`, or automated using a configuration file.

See the :ref:`developer-reference` reference for programmatic use of the commands library.

Generating Commands From a File
-------------------------------

The :py:class:`deployable.commands.scripts.Script` class may instantiate commands by loading a special "steps" configuration
file.

.. code-block:: text

    install apache:
        apt apache

    create the website directory:
        mkdir /var/www/example_com/www
        recursive yes

    set permissions on the website directory:
        perms /var/www/example_com/www
        group www-data
        mode 775
        group www-data

Notes regarding this format:

- The first line is a comment that *must* end with a ``:``.
- All additional lines that pertain to the command *must* be indented using 4 spaces.
- The second line is *always* the command. Arguments, if any, are separated by spaces.
- Additional lines represent keyword arguments with a space between the argument name and the argument value (if any).
- Arguments that should be treated as a single value should be enclosed in double quotes.
- ``yes`` and ``no`` are interpreted as boolean values.
- Simple lists may be represented as ``[one, two, three]``.

Additionally, this file may be pre-processed as a Jinja2 template by providing a context dictionary:

.. code-block:: text

    install apache:
        apt apache

    create the website directory:
        mkdir /var/www/{{ domain_tld }}/www
        recursive yes

    set permissions on the website directory:
        perms /var/www/{{ domain_tld }}/www
        group www-data
        mode 775
        group www-data


Common Options
..............

All commands support the following common options:

- ``environments``: A string or list of strings indicating the operational environments in which the command runs. This
  is *not* used by default, but may be used to programmatically filter commands for a specific environment. For example,
  development versus live.
- ``path``: The path from which a command should be executed.
- ``prefix``: A statement to be added prior to executing the command.
- ``scope``: A string indicating the scope in which the command runs. For example, "provision" versus "deploy". This is
  *not* used by default, but may be used to programmatically filter commands.
- ``shell``: The shell used to run the commands. For example, ``/bin/bash``. This is generally not important, but can
  be a problem when attempting to execute some commands (such as Django management commands).
- ``stop``: A ``yes`` indicates no other commands should be executed if the given command fails.
- ``sudo``: A ``yes`` indicates the command should be automatically prefixed with ``sudo``. If provided as a string, the
  command is also prefixed with a specific user name.

Defining an "Itemized" Command
..............................

Certain command definitions may be repeated by defining a list of items.

Example of an "itemized" command:

.. code-block:: ini

    touch a bunch of files:
        touch /var/www/example_com/www/$item
        items [index.html, assets/index.html, content/index.html]

.. note::
    Not all commands may be itemized. Use preview to test your results.

Available Commands
..................

The following commands instantiate command classes.
Commands with examples:

.. include:: _command-examples.rst
