*************
Configuration
*************

From the Command Line
=====================

Variables INI
=============

Using a Steps File
==================

Variables in the Steps File
---------------------------

Pre-Defined Variables
---------------------

A number of variables are defined at runtime.

Current Host
............

The ``current_host`` variable refers to the Host instance whose roles (and steps) is currently being processed. You may
use this, for example, to acquire the host name or SSH port in your ``steps.cfg`` or template files.

.. code-block:: ini

    # provision/roles/common/steps.cfg
    configure the firewall to allow SSH on port {{ current_host.port }}:
        run "ufw allow {{ current_host.port }}/tcp"
        tags [firewall, security]

Deploy Object
.............

The ``deploy`` object provides a number of variables.

- ``deploy.environment``: The ``environment`` argument provided at the command line. This value is aliased as
  ``deploy.env`` to save some typing.
- ``deploy.release``: The release identifier of the current deployment. This may be provided at the command line or
  automatically read from the ``VERSION.txt`` file in the project's root directory.
- ``deploy.release_path``: The path to where source code is deployed. This takes the form of
  ``<deploy.root>/releases/<release>``.
- ``deploy.root``: The top-most path to where deployment occurs.
- ``deploy.shared_path``: The to where shared files are stored, such as tenant data, log files, etc. This takes the form
  of ``<deploy.root>/shared``>.
- ``deploy.user``: The name of the user authorized to do deployments. Defaults to ``deploy``. Note that this may be
  overriden on a per-host basis in the host INI files.

The deploy object also has a ``start_dt`` and ``end_dt`` indicate the date and time the deployment started and ended.

Hosts
.....

The ``hosts`` variable includes a list of Host objects that are targeted for provisioning or deployment. This may be
useful in templates that need to iterate over the hosts.

.. code-block:: text

    # pg_hba.conf
    {% if deploy.environment == "live" %}
    # TYPE  DATABASE        USER            ADDRESS                 METHOD
    local   all             postgres                                trust
    {% for host in hosts %}
    host    all             postgres        {{ host.ip }}/0               md5
    host    sameuser        all             {{ host.ip }}/0               md5
    {% endfor %}
    {% else %}
    # TYPE  DATABASE        USER            ADDRESS                 METHOD
    local   all             postgres                                trust
    host    all             postgres        localhost               trust
    host    sameuser        all             localhost               md5
    host    sameuser        all             127.0.0.1/32            md5
    {% endif %}

Local Root
..........

The ``local_root`` variable is the path to the project's root directory. It is the absolute path to the current working
directory when the ``deployable`` command is invoked.

Local root may be used when synchronizing the local project to a remote directory, for example, ``deploy.release_path``.

.. code-block:: ini

    push contents of the source directory to the release directory:
        sync "{{ local_root }} {{ deploy.release_path }}"
        delete yes
        links yes
