***********
Conventions
***********

Structure
=========

Project Root
------------

The "root" of the project is the top-level directory of the project where all project-related files are stored. The
``VERSION.txt`` file is at this level, along with the readme file, docs directory, and so on.

The name of this directory is assumed to be the name of the project, and may be used to automatically determine a number
of variables used during deployment. These assumptions may be overridden in the ``_variables.ini`` file.

Deployment Files
----------------

The basic structure expected by the ``deployable`` command requires a ``deployable/`` directory in project root. Each
scope is defined as a sub-directory. The ``hosts/`` directory and ``_variables.ini`` file is also located in this
directory.

Roles and their associated steps and templates are located under ``roles/`` in a separate sub-directory.

.. code-block:: text

    deployable
    |-- _variables.ini
    |-- hosts
    |   `-- live.ini
    |   `-- staging.ini
    `-- <scope>
        `-- roles
            |-- <role>
            |   |-- steps.cfg
                 `-- templates
                    `-- <template_name>


A full example might look like this:

.. code-block:: text

    deployable
    |-- _variables.ini
    |-- deploy
    |   |-- fixtures
    |   |-- requirements
    |   `-- roles
    |       `-- application
    |           |-- steps.cfg
    |           `-- templates
    |               |-- httpd.conf.j2
    |               |-- settings.py.j2
    |               `-- wsgi.py.j2
    |-- hosts
    |   `-- live.ini
    |   `-- staging.ini
    `-- provision
        `-- roles
            |-- apache
            |   `-- steps.cfg
            |-- common
            |   |-- steps.cfg
            |-- pgsql
            |   |-- steps.cfg
            |   `-- templates
            |       `-- pg_hba.conf.j2
            |-- python
            |   `-- steps.cfg
            `-- ssl
                `-- steps.cfg

Authentication and Access
=========================

All steps related to provisioning and deployment are assumed to occur from a *control* machine to one or more remote
hosts. Each remote host is assumed to be configured with a non-root user account -- that is, the *deploy user*.

- The deploy user has limited/specific access to perform actions on the host.
- The public key of the person or agent performing operations must be added to the deploy user's authorized keys.
- The deploy user may perform commands using sudo *without* a password.

Key Files
---------

Key files may be specified for each host, but generally, the same key may be used for all hosts.

Keys may be automatically identified using:

1. The subdomain of the host, if any.
2. The basename of the project root.

Key file names are resolved using the following rules:

1. The key file exists as specified.
2. The named key exists in ``~/.ssh``.
3. The named key exists in ``deployable/keys/``.

User expansion is automatically applied.

Known Hosts
-----------

Rather than updating the ``known_hosts`` file of the local user, SSH commands invoked on the control machine will
automatically store host signatures in ``deployable/.known_hosts``.

Git Ignore
==========

You'll want to add the following to your ``.gitignore`` file:

.. code-block:: text

    deployable/*.log
    deployable/.known_hosts
    deployable/.tmp
    deployable/_variables.ini

If you store SSH key paris in the deployable directory, you'll always want to add ``deployable/keys``.
