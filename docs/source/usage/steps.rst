*****
Steps
*****

A *step* represents one or more commands used to prepare a host to support an environment.

Pre-Defined Steps
=================

The steps library encapsulates common commands in an object-oriented fashion, making it easier to assemble, preview,
and run shell commands, and obtain the results.

Common Options
..............

All steps support the following common options:

- ``cwd``: For "current working directory", the path from which a command should be executed.
- ``environments``: A string or list of strings indicating the operational environments in which the command runs. This
  is used to programmatically filter steps for a specific environment. For example, development versus live.
- ``prefix``: A statement to be executed prior to executing the command.
- ``shell``: The shell used to run the commands. For example, ``/bin/bash``. This is generally not important, but can
  be a problem when attempting to execute some commands (such as Django management commands).
- ``stop``: A ``yes`` indicates no other commands should be executed if the given command fails.
- ``sudo``: A ``yes`` indicates the command should be automatically prefixed with ``sudo``. If provided as a string, the
  command is also prefixed with a specific user name.

Programmatic Use
................

Although the pre-defined steps are designed for use with a ``steps.cfg`` file, they may also be invoked
programmatically. See the :ref:`developer-reference` reference for programmatic use of the steps library.

Using a Steps File
..................

The :py:class:`deployable.steps.factory.Config` class instantiates commands by loading a special "steps" configuration
file.

.. code-block:: text

    install apache:
        apt apache

    create the website directory:
        mkdir /var/www/example_com/www
        recursive yes

    set permissions on the website directory:
        perms /var/www/example_com/www
        group www-data
        mode 775
        group www-data

Notes regarding this format:

- The first line is a comment that *must* end with a ``:``.
- All additional lines that pertain to the command *must* be indented using 4 spaces.
- The second line is *always* the command. Arguments, if any, are separated by spaces.
- Additional lines represent keyword arguments with a space between the argument name and the argument value (if any).
- Arguments that should be treated as a single value should be enclosed in double quotes.
- ``yes`` and ``no`` are interpreted as boolean values.
- Simple lists may be represented as ``[one, two, three]``.

Additionally, this file may be pre-processed as a Jinja2 template by providing a context dictionary:

.. code-block:: text

    install apache:
        apt apache

    create the website directory:
        mkdir /var/www/{{ domain_tld }}/www
        recursive yes

    set permissions on the website directory:
        perms /var/www/{{ domain_tld }}/www
        group www-data
        mode 775
        group www-data

Defining an "Itemized" Step
...........................

Most step definitions may be repeated by defining a list of items.

.. code-block:: ini

    touch a bunch of files:
        touch /var/www/example_com/www/$item
        items [index.html, assets/index.html, content/index.html]

.. note::
    Not all commands may be itemized. Use preview to test your results.

Available Steps
...............

The following definitions instantiate step classes.

.. include:: _step-examples.rst
