#! /bin/bash

# Send stdout and stderror to log.
exec >  >(tee -a /root/stackscript.log)
exec 2> >(tee -a /root/stackscript.log >&2)

# Inputs appear when provisioning the server from a StackScript.
# <UDF name="deploy_root" label="Deployment Root" example="/var/www/example_app" />
# <UDF name="deploy_user" label="Deployment User" example="example_app" />
# <UDF name="deploy_group" label="Deployment Group" default="httpd" />
# <UDF name="deploy_key" label="Deploy Key" example="paste public key here" />
# <UDF name="ssh_port" label="SSH Port" default="22" />

# Functions

function configure_ssh()
{
    header "Configure SSH";

    cp /etc/ssh/sshd_config /etc/ssh/sshd_config.b;
    cat > /etc/ssh/sshd_config << EOF
# DEBUG3 is required to see anything usefull
#LogLevel DEBUG3
Port $SSH_PORT
AddressFamily inet
PermitRootLogin no
PasswordAuthentication no
ChallengeResponseAuthentication no
UsePAM yes
X11Forwarding yes
PrintMotd no
AcceptEnv LANG LC_*
Subsystem       sftp    /usr/lib/openssh/sftp-server
EOF

    systemctl restart sshd.service;
    footer;
}

function create_user()
{
    header "Set Up the Deployment User";

    # Create the deploy root.
    mkdir $DEPLOY_ROOT;

    # On Cent, permissions issues occur with authorized keys when the user isn't in /home. This differs from our other
    # deploys where we locate the user in the same location as the deployed application, e.g. /var/www or /opt.
    # Debian/Ubuntu requires home directory to exist, but it may not be created until apache is installed.
    # Create the user account.
    useradd $DEPLOY_USER;
    usermod -aG wheel $DEPLOY_USER;

    # Create the .ssh directory for the deployment user and add public key.
    mkdir /home/$DEPLOY_USER/.ssh;
    echo "$DEPLOY_KEY" > /home/$DEPLOY_USER/.ssh/authorized_keys;

    # Reset permissions.
    chown -R $DEPLOY_USER /home/$DEPLOY_USER/.ssh;
    chgrp -R $DEPLOY_USER /home/$DEPLOY_USER/.ssh;
    chmod 700 /home/$DEPLOY_USER/.ssh;
    chmod 600 /home/$DEPLOY_USER/.ssh/authorized_keys;

    footer;
}

function header()
{
    local title=$1;
    echo "###############################################################################";
    echo "$title";
    echo "###############################################################################";
}

function footer()
{
    echo "-------------------------------------------------------------------------------";
    echo "";
}

function reset_permissions()
{
    # Set permissions on the deployment directory. This must be done after installing Apache in order to correct
    # permissions for both Apache and the Deployment user.
    header "Reset Permissions for Deployment User/Path";
    chown -R $DEPLOY_USER $DEPLOY_ROOT;
    chgrp -R $DEPLOY_GROUP $DEPLOY_ROOT;
    chmod -R 755 $DEPLOY_ROOT;
    footer;
}

function update_system()
{
    header "Update/Upgrade the System";
    yum -y update;
    yum -y install yum-utils;

    # EPEL may be needed for various package installs.
    yum -y install epel-release;
    footer;
}

# Procedure

update_system;
create_user;
configure_ssh;
reset_permissions;
