# Provision Cent OS

## About

This StackScript may be used to create a new Linode for Cent OS 7. It will:

1. Create and boot the server, running update/upgrade when the server has finished booting.
2. Add a user account that may be used for further provisioning and deployment.
3. Set up SSH for key authentication.

The script will logs errors and output to ``/root/stackscript.log``.

## Usage

1. Go to the Linode management area and click on StackScripts.
2. Create a new script selecting Debian and/or Ubuntu as the supported distribution.
3. Copy/paste the contents of the script.

You may now use the script to create a new Linode.

## Key Authentication

To create a Linode using this script, you must have an SSH key: 

    ssh-keygen -t rsa -C "deploy@example.com" -f ~/.ssh/example_com; # don't enter a password
    chmod 600 ~/.ssh/example_com.pub;

> Tip: You may copy the key like so: ``pbcopy < ~/.ssh/example_com.pub``

## Terms and Definitions

*deploy group*: This is the group that requires read access to the deploy root. Usually, this is the group in which the 
web server user resides.

*deploy key*: The public key added to ``authorized_users`` of the deploy user.

*deploy root*: The top-most path to deployed assets and software.

*deploy user*: The limited-privilege user account authorized to perform deployment actions on a server.
