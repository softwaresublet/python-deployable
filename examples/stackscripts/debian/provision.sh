#! /bin/bash

# Send stdout and stderror to log.
exec >  >(tee -a /root/stackscript.log)
exec 2> >(tee -a /root/stackscript.log >&2)

# Stop those annoying prompts during provisioning.
export DEBIAN_FRONTEND=noninteractive;

# Inputs appear when provisioning the server from a StackScript.
# <UDF name="deploy_root" label="Deployment Root" example="/var/www/example_app" />
# <UDF name="deploy_user" label="Deployment User" example="example_app" />
# <UDF name="deploy_group" label="Deployment Group" default="www-data" />
# <UDF name="deploy_key" label="Deploy Key" example="paste public key here" />
# <UDF name="ssh_port" label="SSH Port" default="22" />

# Functions

function configure_firewall()
{
    header "Install and Configure Security Measures";

    feedback "Set Up Fail2Ban";
    apt-get install -y fail2ban;

    feedback "Configure Firewall";
    ufw allow 4894/tcp; # allow ssh
    ufw allow http;
    ufw allow https;
    ufw enable;

#    feedback "Install chkrootkit";
#    (cd /tmp && wget --passive-ftp ftp://ftp.pangeia.com.br/pub/seg/pac/chkrootkit.tar.gz);
#    (cd /tmp && tar -xzf chkrootkit.tar.gz);
#    (cd /tmp/chkrootkit-*/ && make sense);
#    rm -Rf /tmp/chkrootkit*;
#
#    feedback "Install BFD: https://www.rfxn.com/projects/brute-force-detection/";
#    (cd /tmp && wget https://www.rfxn.com/downloads/bfd-current.tar.gz);
#    (cd /tmp && tar -xzf bfd-current.tar.gz);
#    (cd /tmp/bfd-1*/ && ./install.sh);
#    rm -Rf /tmp/bfd-*;
#
#    feedback "Install DDos Deflate: https://github.com/jgmdev/ddos-deflate";
#    (cd /tmp && wget https://github.com/jgmdev/ddos-deflate/archive/master.zip);
#    (cd /tmp && unzip master.zip);
#    (cd /tmp/ddos-deflate-master && ./install.sh);
#    rm -f /tmp/master.zip;
#    rm -Rf /tmp/ddos-deflate-master;

    footer;
}

function configure_ssh()
{
    header "Configure SSH";

    cp /etc/ssh/sshd_config /etc/ssh/sshd_config.b;
    cat > /etc/ssh/sshd_config << EOF
# DEBUG3 is required to see anything usefull
#LogLevel DEBUG3
Port $SSH_PORT
AddressFamily inet
PermitRootLogin no
PasswordAuthentication no
ChallengeResponseAuthentication no
UsePAM yes
X11Forwarding yes
PrintMotd no
AcceptEnv LANG LC_*
Subsystem       sftp    /usr/lib/openssh/sftp-server
EOF

    service ssh restart;
    footer;
}

function create_user()
{
    header "Set Up the Deployment User";

    # The gecos switch eliminates the prompts.
    adduser $DEPLOY_USER --disabled-password --gecos "" --home $DEPLOY_ROOT;
    adduser $DEPLOY_USER sudo;

    # Create sudo file for the deployment user.
    echo "$DEPLOY_USER ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/$DEPLOY_USER;

    # Create the .ssh directory for the deployment user.
    mkdir $DEPLOY_ROOT/.ssh;

    # Add public key for the deployment user.
    echo "$DEPLOY_KEY" > $DEPLOY_ROOT/.ssh/authorized_keys;

    footer;
}

function header()
{
    local title=$1;
    echo "###############################################################################";
    echo "$title";
    echo "###############################################################################";
}

function footer()
{
    echo "-------------------------------------------------------------------------------";
    echo "";
}

function reset_permissions()
{
    # Set permissions on the deployment directory. This must be done after installing Apache in order to correct
    # permissions for both Apache and the Deployment user.
    header "Reset Permissions for Deployment User/Path";
    chown -R $DEPLOY_USER $DEPLOY_ROOT;
    chgrp -R $DEPLOY_GROUP $DEPLOY_ROOT; # or leave it as www-data?
    chmod -R 755 $DEPLOY_ROOT;
    chmod 700 $DEPLOY_ROOT/.ssh;
    chmod 600 $DEPLOY_ROOT/.ssh/authorized_keys;
    footer;
}

function update_system()
{
    header "Update/Upgrade the System";
    apt-get update;
    apt-get upgrade -y;
    apt-get install -y aptitude;
    footer;
}

# Procedure

update_system;
create_user;
configure_ssh;
configure_firewall;
reset_permissions;
