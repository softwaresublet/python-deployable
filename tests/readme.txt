Directory of unit tests. Tests are executed above this directory.

## Running Tests

> Use ``make tests`` to run tests with coverage added to ``docs/build/html/coverage/``.

To run unit tests:

    python -m unittest discover;

To run a specific test:

    python -m unittest tests.test_name;

Running tests with coverage:

    coverage run --source=. -m unittest discover;

Reviewing the coverage report:

    coverage report -m;

Reviewing the HTML coverage report:

    coverage html;
    open htmlcov/index.html;

## Test Data

### Example Sites

``example_com`` is typical, model site with no errors.

``example_net`` is a copy of ``example_com``, but contains additional files that produce errors for testing.

``example_org`` is generated and removed during testing.

### tmp

The ``tmp/`` directory may be created and removed during testing.
