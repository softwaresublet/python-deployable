from deployable.commands import Command, Result
from deployable.constants import EXIT_OK, EXIT_UNKNOWN
from deployable.hosts import Host
from deployable.ssh import Client
import unittest

# Tests


class TestCommand(unittest.TestCase):

    def test_get_result(self):
        command = Command("ls -ls", local=True, sudo=False)
        command.run()

        result = command.get_result()
        self.assertIsInstance(result, Result)
        self.assertFalse(result.failure)
        self.assertTrue(result.success)

        command = Command("ls -ls nonexistent", local=True, sudo=False)
        command.run()

        result = command.get_result()
        self.assertIsInstance(result, Result)
        self.assertTrue(result.failure)
        self.assertFalse(result.success)

    def test_preview(self):
        """Test the preview output of a command."""
        command = Command("ls -ls", sudo=False)
        self.assertEqual("ls -ls", command.preview())

        command = Command("ls -ls", cwd="deployable")
        self.assertEqual("(cd deployable && sudo ls -ls)", command.preview())

        command = Command("ls -ls", prefix='echo "testing"', sudo=False)
        self.assertEqual('echo "testing" && ls -ls', command.preview())

        command = Command("ls -ls", cwd="deployable", prefix='echo "testing"')
        self.assertEqual('(cd deployable && echo "testing" && sudo ls -ls)', command.preview())

    # TODO: Test _remote() command execution.
    def test_remote(self):
        # Can't execute without a client instance.
        command = Command("uname")
        self.assertFalse(command.run())

        # Connect with a bad client.
        host = Host("localhost")
        client = Client(host)
        command.client = client
        self.assertFalse(command.run())
        self.assertIsNotNone(command.error)

        # Connect with a good host.
        # TODO: Create a host for testing deployable connections.
        host = Host(
            "testing.deployablehq.com",
            ip="104.200.29.159",
            key_file="~/.ssh/sandbox",
            port=4894,
            user="sandbox"
        )
        client = Client(host)
        command.client = client
        result = command.run()
        self.assertTrue(result)
        self.assertIsNotNone(command.output)

        # Run a command that files.
        command = Command("ls -ls nonexistent")
        command.client = client
        result = command.run()
        self.assertFalse(result)
        self.assertIsNotNone(command.error)

        # TODO: How to force an SSH exception. Paramiko.channel.exec_command() says "if the request was rejected or the
        # channel was closed"

    def test_repr(self):
        """Test the representation of a command."""
        command = Command("ls -ls")
        self.assertEqual("<Command: ls -ls>", repr(command))

    def test_run(self):
        """Test running a command."""
        command = Command("ls -ls", cwd="deployable", local=True, prefix='echo "testing"', sudo=False)
        command.run()
        self.assertEqual(EXIT_OK, command.code)

        command = Command("ls -ls", cwd="nonexistent", local=True, sudo=False)
        command.run()
        self.assertEqual(EXIT_UNKNOWN, command.code)

    def test_string(self):
        """Test the string output of a command."""
        command = Command("ls -ls", sudo=False)
        self.assertEqual("ls -ls", str(command))

    def test_sudo(self):
        command = Command("ls -ls", sudo=True)
        self.assertEqual("sudo ls -ls", command.preview())

        command = Command("ls -ls", sudo=False)
        self.assertEqual("ls -ls", command.preview())

        command = Command("ls -ls", sudo="bob")
        self.assertEqual("sudo -u bob ls -ls", command.preview())
