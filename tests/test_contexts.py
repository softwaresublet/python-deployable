import os
import unittest
from deployable.contexts import Context, initialize_context
from deployable.variables import Variable

# TODO: Switch context EXAMPLES_PATH to example_app _variables.ini file.
EXAMPLES_PATH = os.path.join("tests", "examples")

# Tests


class TestContext(unittest.TestCase):

    def test_append(self):
        context = Context()
        context.append(Variable("testing", value=1))

        self.assertEqual(1, len(context._attributes))

    def test_from_ini(self):
        context = Context.from_ini(os.path.join(EXAMPLES_PATH, "_variables.ini"))

        self.assertEqual(5, len(context._attributes))

    def test_get(self):
        context = Context()
        context.add("testing", 1)

        self.assertEqual(1, context.get("testing"))
        self.assertEqual(True, context.get("nonexistent", default=True))

    def test_getattr(self):
        context = Context()
        context.add("testing", 1)

        self.assertEqual(1, context.testing)

        self.assertIsNone(context.nonexistent)

    def test_has(self):
        context = Context()
        context.add("testing", 1)

        self.assertTrue(context.has("testing"))
        self.assertFalse(context.has("nonexistent"))


class TestInitializeContext(unittest.TestCase):

    def test_variables_from_command_line(self):
        variables = [
            "testing:yes",
            "fun:no",
        ]
        context = initialize_context("staging", path=EXAMPLES_PATH, variables_from_command_line=variables)
        self.assertEqual(5, len(context._attributes))

    def test_variables_from_file(self):
        context = initialize_context("staging", path=EXAMPLES_PATH)
        self.assertEqual(3, len(context._attributes))

        context = initialize_context("live", path=EXAMPLES_PATH)
        self.assertEqual(4, len(context._attributes))

        context = initialize_context("development", path=EXAMPLES_PATH)
        self.assertEqual(3, len(context._attributes))
