from datetime import datetime
from deployable.deployments import Deploy
from deployable.constants import EXIT_OK, EXIT_UNKNOWN
import unittest

# Tests


class TestDeployment(unittest.TestCase):

    def test_end(self):
        deploy = Deploy("testing", "/var/www/example_app", "1.1")
        deploy.end()
        self.assertIsNotNone(deploy.end_dt)
        self.assertIsInstance(deploy.end_dt, datetime)

    def test_repr(self):
        """Test the representation of a deploy instance.."""
        deploy = Deploy("testing", "/var/www/example_app", "1.1")
        self.assertEqual("<Deploy testing:/var/www/example_app>", repr(deploy))

    def test_start(self):
        deploy = Deploy("testing", "/var/www/example_app", "1.1")
        deploy.start()
        self.assertIsNotNone(deploy.start_dt)
        self.assertIsInstance(deploy.start_dt, datetime)
