from deployable.commands import Command
from deployable.discovery import Discovery
from deployable.hosts import Host
from unittest import mock
import unittest

# TODO: Create a host for testing deployable connections.
host = Host(
    "testing.deployablehq.com",
    ip="104.200.29.159",
    key_file="~/.ssh/sandbox",
    port=4894,
    user="sandbox"
)

PYTHON_2 = "Python 2.7.15rc1"
PYTHON_3 = "3.7.0"

PLATFORM_CENTOS = "Linux-3.10.0-862.11.6.el7.x86_64-x86_64-with-centos-7.5.1804-Core"
PLATFORM_DARWIN = "Darwin-17.7.0-x86_64-i386-64bit"
PLATFORM_DEBIAN = "Linux-4.15.0-32-generic-x86_64-with-Debian-18.04-bionic"
PLATFORM_FEDORA = "Linux-4.15.0-32-generic-x86_64-with-Fedora-18.04-bionic"
PLATFORM_REDHAT = "Linux-4.15.0-32-generic-x86_64-with-RedHat-18.04-bionic"
PLATFORM_UBUNTU = "Linux-4.15.0-32-generic-x86_64-with-Ubuntu-18.04-bionic"
PLATFORM_UNKNOWN = "Unknown"
PLATFORM_WINDOWS = "Windows"

LINUX_PLATFORMS = (
    PLATFORM_CENTOS,
    PLATFORM_DEBIAN,
    PLATFORM_FEDORA,
    PLATFORM_REDHAT,
    PLATFORM_UBUNTU,
)

# Tests


class TestDiscovery(unittest.TestCase):

    def test_get_distribution(self):
        d = Discovery(host)

        d.platform = PLATFORM_CENTOS
        self.assertEqual("centos", d.get_distribution())

        d.platform = PLATFORM_DARWIN
        self.assertEqual("darwin", d.get_distribution())

        d.platform = PLATFORM_DEBIAN
        self.assertEqual("debian", d.get_distribution())

        d.platform = PLATFORM_FEDORA
        self.assertEqual("fedora", d.get_distribution())

        d.platform = PLATFORM_REDHAT
        self.assertEqual("redhat", d.get_distribution())

        d.platform = PLATFORM_UBUNTU
        self.assertEqual("ubuntu", d.get_distribution())

        d.platform = PLATFORM_WINDOWS
        self.assertEqual("windows", d.get_distribution())

        d.platform = PLATFORM_UNKNOWN
        self.assertEqual("unknown", d.get_distribution())

    def test_get_configuration_path(self):
        d = Discovery(host)

        for p in LINUX_PLATFORMS:
            d.platform = p
            self.assertEqual("~/.config", d.get_configuration_path())

        d.platform = PLATFORM_DARWIN
        self.assertEqual("~/Library/Application Support", d.get_configuration_path())

        d.platform = PLATFORM_WINDOWS
        self.assertEqual("~/AppData/Roaming", d.get_configuration_path())

        d.platform = PLATFORM_UNKNOWN
        self.assertIsNone(d.get_configuration_path())

    def test_get_temp_path(self):
        d = Discovery(host)

        for p in LINUX_PLATFORMS:
            d.platform = p
            self.assertEqual("~/.cache", d.get_temp_path())

        d.platform = PLATFORM_DARWIN
        self.assertEqual("~/Library/Caches", d.get_temp_path())

        d.platform = PLATFORM_WINDOWS
        self.assertEqual("~/AppData/Locals", d.get_temp_path())

        d.platform = PLATFORM_UNKNOWN
        self.assertIsNone(d.get_temp_path())

    def test_is_linux(self):
        d = Discovery(host)

        for p in LINUX_PLATFORMS:
            d.platform = p
            self.assertTrue(d.is_linux)

        d.platform = PLATFORM_DARWIN
        self.assertFalse(d.is_linux)

        d.platform = PLATFORM_WINDOWS
        self.assertFalse(d.is_linux)

        d.platform = PLATFORM_UNKNOWN
        self.assertFalse(d.is_linux)

    def test_is_osx(self):
        d = Discovery(host)

        d.platform = PLATFORM_DARWIN
        self.assertTrue(d.is_osx)

        for p in LINUX_PLATFORMS:
            d.platform = p
            self.assertFalse(d.is_osx)

        d.platform = PLATFORM_WINDOWS
        self.assertFalse(d.is_osx)

        d.platform = PLATFORM_UNKNOWN
        self.assertFalse(d.is_osx)

    def test_is_python2(self):
        d = Discovery(host)

        d.python_version = PYTHON_2
        self.assertTrue(d.is_python2)
        self.assertFalse(d.is_python3)

    def test_is_python3(self):
        d = Discovery(host)

        d.python_version = PYTHON_3
        self.assertTrue(d.is_python3)
        self.assertFalse(d.is_python2)

    def test_is_windows(self):
        d = Discovery(host)

        d.platform = PLATFORM_WINDOWS
        self.assertTrue(d.is_windows)

        d.platform = PLATFORM_DARWIN
        self.assertFalse(d.is_windows)

        for p in LINUX_PLATFORMS:
            d.platform = p
            self.assertFalse(d.is_windows)

        d.platform = PLATFORM_UNKNOWN
        self.assertFalse(d.is_windows)

    def test_load(self):
        # A bad host fails to run the platform command and therefore returns False.
        bad_host = Host(
            "localhost",
            key_file="~/.ssh/sandbox",
            port=4894,
            user="sandbox"
        )
        d = Discovery(bad_host)
        self.assertFalse(d.load())

        # def _remote():
        #     return True
        #
        # with mock.patch(Command._remote, new_callable=_remote):
        #

