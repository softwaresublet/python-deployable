from myninjas.context_managers import cd
import os
import unittest
from deployable.contexts import Context
from deployable.hosts import factory, Host

EXAMPLES_PATH = os.path.join("tests", "example_app")

# Tests


class TestFactory(unittest.TestCase):

    def test_factory(self):
        path = os.path.join("deployable", "hosts", "testing.ini")
        with cd(EXAMPLES_PATH):
            hosts = factory(path)

        self.assertEqual(2, len(hosts))

        self.assertEqual(5, len(hosts[0].roles))
        self.assertEqual(2, len(hosts[1].roles))

        self.assertTrue(type(hosts[0].port) == int)
        self.assertEqual(10022, hosts[0].port)

        self.assertTrue(type(hosts[1].port) == int)
        self.assertEqual(22, hosts[1].port)

        self.assertEqual("deployable/keys/fake", hosts[0].key_file)
        self.assertIsNone(hosts[1].key_file)

        self.assertEqual("deployablehq_com", hosts[0].user)
        self.assertEqual("deployablehq_com", hosts[1].user)


class TestHost(unittest.TestCase):

    def test_address(self):
        host = Host("testing.deployablehq.com")
        self.assertEqual("testing.deployablehq.com", host.address)

        host = Host("testing.deployablehq.com", ip="1.2.3.4")
        self.assertEqual("1.2.3.4", host.address)

    def test_getattr(self):
        host = Host("testing.deployablehq.com", testing=True)
        self.assertTrue(host.testing)

    def test_guess_user_name(self):
        host = Host("testing.deployablehq.com")
        self.assertEqual("deployablehq_com", host.guess_user_name())

        host = Host("another.testing.deployablehq.com")
        self.assertEqual("testing_deployablehq_com", host.guess_user_name())

        host = Host("deployablehq.com")
        self.assertEqual("deployablehq_com", host.guess_user_name())

    def test_repr(self):
        host = Host("testing.deployablehq.com")
        self.assertEqual("<Host testing.deployablehq.com>", repr(host))
