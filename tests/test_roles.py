from myninjas.context_managers import cd
import os
import unittest
from deployable.contexts import Context
from deployable.deployments import Deploy
from deployable.hosts import Host
from deployable.roles import factory, Role
from deployable.steps.base import ItemizedStep, Step

EXAMPLES_PATH = os.path.join("tests", "example_app")

# Tests


class TestFactory(unittest.TestCase):

    def test_default_path(self):
        """Load a role on the default path."""
        deploy = Deploy("testing", "/var/www/deployablehq_com", "1.1", user="deployablehq_com")

        host = Host("testing.deployablehq.com")

        context = Context()
        context.set("current_host", host)
        context.add("deploy", deploy)
        context.add("local_root", EXAMPLES_PATH)

        with cd(EXAMPLES_PATH):
            role = factory("application", context=context)
            self.assertIsInstance(role, Role)

        path = os.path.join(EXAMPLES_PATH, "deployable", "deploy")
        role = factory("application", context=context, path=path)
        self.assertIsInstance(role, Role)

        self.assertEqual("tests/example_app/deployable/deploy/roles/application", role.path)

    def test_nonexistent_role(self):
        """Check that None is returned when a bad path is given to the factory."""
        role = factory("nonexistent")
        self.assertIsNone(role)


class TestRole(unittest.TestCase):

    def test_iter(self):
        deploy = Deploy("testing", "/var/www/deployablehq_com", "1.1", user="deployablehq_com")

        host = Host("testing.deployablehq.com")

        context = Context()
        context.set("current_host", host)
        context.add("deploy", deploy)
        context.add("local_root", EXAMPLES_PATH)

        with cd(EXAMPLES_PATH):
            role = factory("application", context=context)

            for step in role:
                self.assertTrue(isinstance(step, ItemizedStep) or isinstance(step, Step))

    def test_repr(self):
        role = Role("testing", "/path/to/testing")
        self.assertEqual("<Role testing>", repr(role))
