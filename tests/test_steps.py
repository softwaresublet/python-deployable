from myninjas.context_managers import captured_output
from deployable.commands import Command
from deployable.constants import EXIT_OK, EXIT_UNKNOWN
from deployable.deployments import Deploy
from deployable.steps.base import ItemizedStep, Step
from deployable.steps.factory import factory
from deployable.steps.library.apache import *
from deployable.steps.library.django import Django
from deployable.steps.library.files import *
from deployable.steps.library.generic import *
from deployable.steps.library.installers import *
from deployable.steps.library.messages import Message
from deployable.steps.library.pgsql import *
from deployable.steps.library.rsync import *
from deployable.steps.library.services import *
from deployable.steps.library.ssl import *
from deployable.steps.library.tarball import *
from deployable.steps.library.templates import *
from deployable.steps.library.users import *
from deployable.steps.mappings import mapping_exists
import os
import unittest

# Tests


class TestApache(unittest.TestCase):

    def test_config_test(self):
        c = ConfigTest()
        self.assertEqual("sudo apachectl configtest;", c.preview())

    def test_disable_module(self):
        c = DisableModule("ssl")
        self.assertEqual("sudo a2dismod ssl;", c.preview())

    def test_disable_site(self):
        c = DisableSite("000-default.conf")
        self.assertEqual("sudo a2dissite 000-default.conf;", c.preview())

        c = DisableSite("example.com")
        self.assertEqual("sudo a2dissite example.com.conf;", c.preview())

    def test_enable_module(self):
        c = EnableModule("rewrite")
        self.assertEqual("sudo a2enmod rewrite;", c.preview())

        args = ["$item"]
        kwargs = {
            'items': ["rewrite", "ssl", "wsgi"],
        }
        c = factory("apache.enable_module", *args, **kwargs)
        # c = EnableModule("$item", items=items)
        p = c.preview()
        self.assertTrue("rewrite" in p)
        self.assertTrue("ssl" in p)
        self.assertTrue("wsgi" in p)

    def test_enable_site(self):
        c = EnableSite("example.com")
        self.assertEqual("sudo a2ensite example.com.conf;", c.preview())

        c = EnableSite("example.com.conf")
        self.assertEqual("sudo a2ensite example.com.conf;", c.preview())


class TestItemizedStep(unittest.TestCase):

    def test_init(self):
        args = ["/var/www/example_com/www/$item"]
        items = ["_assets", "_content", "_includes"]
        c = ItemizedStep(MakeDir, items, "create multiple directories", *args)
        self.assertEqual(3, len(c.get_commands()))

        c = ItemizedStep(MakeDir, items, "this gets replace by comment", *args, comment="create multiple directories")
        self.assertEqual("create multiple directories", c.name)

    def test_repr(self):
        args = ["/path/to/$item"]
        items = ["www", "www/_assets", "www/_content"]
        c = ItemizedStep(MakeDir, items, "create multiple directories", *args)
        self.assertEqual("<ItemizedStep MakeDir>", repr(c))


class TestDjango(unittest.TestCase):

    def test_manage(self):
        c = Django("collectstatic", cwd="/path/to/example_com", prefix="source ../python/bin/activate")
        o = "(cd /path/to/example_com && source ../python/bin/activate && sudo ./manage.py collectstatic);"
        self.assertEqual(o, c.preview())

    def test_settings(self):
        c = Django("migrate", cwd="/path/to/example_com", prefix="source ../python/bin/activate",
                   settings="tenants.acme_example_app.settings")
        o = "(cd /path/to/example_com && source ../python/bin/activate && sudo ./manage.py migrate --settings=tenants.acme_example_app.settings);"
        self.assertEqual(o, c.preview())


class TestFiles(unittest.TestCase):

    def test_append(self):
        c = Append("/path/to/file.txt", content="This is a test.", sudo=False)
        o = 'echo "This is a test." >> /path/to/file.txt;'
        self.assertEqual(o, c.preview())

    def test_copy(self):
        c = Copy("/path/to/dir/", "/path/to/other/", recursive=True)
        o = "sudo cp -R /path/to/dir/ /path/to/other/;"
        self.assertEqual(o, c.preview())

        c = Copy("/path/to/file.txt", "/path/to/other.txt")
        o = "sudo cp /path/to/file.txt /path/to/other.txt;"
        self.assertEqual(o, c.preview())

    def test_makedir(self):
        c = MakeDir("/path/to/dir", mode="755", recursive=True)
        o = "sudo mkdir -m 755 -p /path/to/dir;"
        self.assertEqual(o, c.preview())

        c = MakeDir("/path/to/dir", mode="755", recursive=False)
        o = "sudo mkdir -m 755 /path/to/dir;"
        self.assertEqual(o, c.preview())

    def test_permissions(self):
        c = Permissions("/path/to/dir", group="www-data", mode="755", owner="bob", recursive=True)

        self.assertEqual(3, len(c.get_commands()))

        p = c.preview()
        self.assertTrue("chgrp" in p)
        self.assertTrue("chown" in p)
        self.assertTrue("chmod" in p)

        c = Permissions(os.path.join("tests", "data", "example.csv"), local=True, mode=775, sudo=False)
        results = list()
        for command in c.get_commands():
            results.append(command.run())

        self.assertTrue(all(results))

    def test_remove(self):
        c = Remove("/path/to/file.txt", force=True, recursive=True)
        o = "sudo rm -f -r /path/to/file.txt;"
        self.assertEqual(o, c.preview())

    def test_sed(self):
        c = Sed("/path/to/file.txt", find="foo", replace="bar")
        o = "sudo sed -i .b 's/foo/bar/g' /path/to/file.txt;"
        self.assertEqual(o, c.preview())

    def test_symlink(self):
        c = Symlink("/path/to/file.txt", force=True)
        o = "sudo ln -s -f /path/to/file.txt file.txt;"
        self.assertEqual(o, c.preview())

        c = Symlink("/path/to/file.txt")
        o = "sudo ln -s /path/to/file.txt file.txt;"
        self.assertEqual(o, c.preview())

    def test_touch(self):
        c = Touch("/path/to/file.txt")
        self.assertEqual("sudo touch /path/to/file.txt;", c.preview())


class TestInstallers(unittest.TestCase):

    def test_apt(self):
        c = Apt("apache2")
        self.assertEqual("sudo apt-get install -y apache2;", c.preview())

    def test_pip(self):
        c = Pip("django")
        self.assertEqual("sudo pip3 install django;", c.preview())

    def test_virtualenv(self):
        c = VirtualEnv("python")
        self.assertEqual("sudo virtualenv python;", c.preview())

    def test_yum(self):
        c = Yum("php7")
        self.assertEqual("sudo yum -y install php7;", c.preview())


class TestMapping(unittest.TestCase):

    def test_mapping_exists(self):
        self.assertTrue(mapping_exists("run"))
        self.assertFalse(mapping_exists("nonexistent"))


class TestMessages(unittest.TestCase):

    def test_message(self):
        c = Message("This is a test.", sudo=False)
        o = 'echo "This is a test.";'
        self.assertEqual(o, c.preview())

    # def test_slack(self):
    #     c = Slack("This is a test.", "asdf1234")
    #     self.assertEqual("https://slack.com/url/to/be/determined", c.preview())


class TestPostgres(unittest.TestCase):

    def test_create_database(self):
        c = CreateDatabase("example_app", admin_pass="testpass", owner="example_app")
        o = 'export PGPASSWORD="testpass" && createdb -h localhost -U postgres -O example_app example_app;'
        self.assertEqual(o, c.preview())

        c = CreateDatabase("example_app", owner="example_app")
        o = "createdb -h localhost -U postgres -O example_app example_app;"
        self.assertEqual(o, c.preview())

    def test_create_user(self):
        c = CreateUser("example_app", admin_pass="adminpass", password="testpass")
        statements = c.preview().split("\n")

        self.assertEqual(
            'export PGPASSWORD="adminpass" && createuser -h localhost -U postgres -DRS example_app;',
            statements[0]
        )

        self.assertEqual(
            'psql -h localhost -U postgres -c "ALTER USER example_app WITH ENCRYPTED PASSWORD \'testpass\';";',
            statements[1]
        )

    def test_database_exists(self):
        c = DatabaseExists("example_app")
        o = "psql -h localhost -U postgres -lqt | cut -d \| -f 1 | grep -qw example_app;"
        self.assertEqual(o, c.preview())

    def test_drop_database(self):
        c = DropDatabase("example_app", admin_pass="testpass")
        o = 'export PGPASSWORD="testpass" && dropdb -h localhost -U postgres example_app;'
        self.assertEqual(o, c.preview())

    def test_drop_user(self):
        c = DropUser("example_app", admin_pass="testpass")
        o = "export PGPASSWORD=\"testpass\" && dropuser -h localhost -U postgres example_app;"
        self.assertEqual(o, c.preview())

    def test_dump_database(self):
        c = DumpDatabase("example_app", admin_pass="testpass")
        o = 'export PGPASSWORD="testpass" && pg_dump -h localhost -U postgres -f example_app.sql example_app;'
        self.assertEqual(o, c.preview())

    def test_psql(self):
        c = PSQL("SELECT * FROM users;", database="example_app", password="testpass", user="example_apps")
        o = 'export PGPASSWORD="testpass" && psql -h localhost -U example_apps -d example_app -c "SELECT * FROM users;";'
        self.assertEqual(o, c.preview())


class TestRsync(unittest.TestCase):

    def test_rysnc_remote(self):
        # First with specified host, key, etc.
        c = Rsync(
            "/path/to/source",
            "/path/to/remote/",
            delete=True,
            host="127.0.0.1",
            key_file="~/.ssh/example",
            links=True,
            recursive=True,
            user="bob"
        )
        o = 'rsync -e "ssh -i /Users/shawn/.ssh/example -p 22" --checksum --compress --copy-links --cvs-exclude --delete -P --recursive /path/to/source bob@127.0.0.1:/path/to/remote/;'
        self.assertEqual(o, c.preview())

        # Next without specified host, key, etc.
        c = Rsync(
            "/path/to/example.com",
            "/path/to/example_com/",
            delete=True,
            links=True,
            recursive=True
        )
        o = 'rsync -e "ssh -i /Users/shawn/.ssh/example.com -p 22" --checksum --compress --copy-links --cvs-exclude --delete -P --recursive /path/to/example.com example.com@example.com:/path/to/example_com/;'
        self.assertEqual(o, c.preview())

    def test_rsync_local(self):
        c = Rsync(
            "source",
            "/path/to/target",
            cwd="/path/to",
            delete=True,
            links=True,
            local=True,
            recursive=True
        )
        o = '(cd /path/to && rsync --checksum --compress --copy-links --cvs-exclude --delete -P --recursive source /path/to/target);'
        self.assertEqual(o, c.preview())

        c = Rsync(
            "/path/to/source",
            "/path/to/target",
            delete=True,
            links=True,
            local=True,
            recursive=True
        )
        o = 'rsync --checksum --compress --copy-links --cvs-exclude --delete -P --recursive /path/to/source /path/to/target;'
        self.assertEqual(o, c.preview())


class TestServices(unittest.TestCase):

    def test_reload(self):
        c = Reload("apache2")
        o = "sudo service apache2 reload;"
        self.assertEqual(o, c.preview())

    def test_restart(self):
        c = Restart("apache2")
        o = "sudo service apache2 restart;"
        self.assertEqual(o, c.preview())

    def test_start(self):
        c = Start("apache2")
        o = "sudo service apache2 start;"
        self.assertEqual(o, c.preview())

    def test_stop(self):
        c = Stop("apache2")
        o = "sudo service apache2 stop;"
        self.assertEqual(o, c.preview())


class TestSSL(unittest.TestCase):

    def test_certbot(self):
        c = Certbot("example.com", email="webmaster@example.com", webroot="/path/to/www")
        o = "sudo certbot certonly --agree-tos --email webmaster@example.com -n --webroot -w /path/to/www -d example.com;"
        self.assertEqual(o, c.preview())


class TestStep(unittest.TestCase):

    def test_get_commands(self):
        step = Step("testing")
        self.assertIsInstance(step.get_commands(), list)
        self.assertEqual(0, len(step.get_commands()))

    def test_get_count(self):
        step = Step("testing")
        self.assertIsInstance(step.get_count(), int)
        self.assertEqual(0, step.get_count())

    def test_repr(self):
        step = Step("testing")
        self.assertEqual("<Step testing>", repr(step))

    # TODO: Test Step.run() if this is going to be used..
    def test_run(self):
        pass


class TestTarball(unittest.TestCase):

    def test_archive(self):
        c = Archive(
            "/path/to/example_app",
            absolute=True,
            exclude="*.tmp",
            strip=1,
            view=True
        )
        o = "sudo tar -czPv --exclude *.tmp --strip-components 1 -f ./archive.tgz /path/to/example_app;"
        self.assertEqual(o, c.preview())

    def test_extract(self):
        c = Extract(
            "/path/to/archive.tgz",
            absolute=True,
            exclude="*.log",
            strip=1,
            view=True
        )
        o = "sudo tar -xzPv --exclude *.log --strip-components 1 -f /path/to/archive.tgz .;"
        self.assertEqual(o, c.preview())


class TestTemplate(unittest.TestCase):

    def test_init(self):
        # First a normal command.
        command = Template(
            "httpd.conf.j2",
            "example.com.conf",
            locations=[
                os.path.join("tests", "example_app", "deployable", "deploy", "roles", "application")
            ]
        )
        self.assertIsNone(command._command_kwargs['cwd'])

        # Next init with cwd.
        command = Template(
            "httpd.conf.j2",
            "example.com.conf",
            cwd="/path/to/nonexistent",
            locations=[
                os.path.join("tests", "example_app", "deployable", "deploy", "roles", "application")
            ]
        )
        self.assertEqual("/path/to/nonexistent", command._command_kwargs['cwd'])

    def test_get_commands(self):
        command = Template(
            "httpd.conf.j2",
            "example.com.conf",
            locations=[
                os.path.join("tests", "example_app", "deployable", "deploy", "roles", "application", "templates")
            ]
        )
        self.assertEqual(2, len(command.get_commands()))
        self.assertIsInstance(command.get_commands()[1], TemplateCommand)


class TestTemplateCommand(unittest.TestCase):

    def setUp(self):

        class Page(object):
            content = "This is a test."

        self.page_class = Page

    def test_get_content(self):
        deploy = Deploy("testing", "/var/www/example_app", "1.1", user="example_app")
        context = {
            'deploy': deploy,
            'domain_name': "example.app",
        }

        # First with a template that exists.
        command = TemplateCommand(
            "httpd.conf.j2",
            "example.com.conf",
            context=context,
            locations=[
                os.path.join("tests", "example_app", "deployable", "deploy", "roles", "application", "templates")
            ]
        )
        content = command.get_content()
        self.assertTrue("example.app" in content)
        self.assertTrue("example_app" in content)

        # Next with a template that does not exist.
        command = TemplateCommand(
            "nonexistent.conf.j2",
            "example.com.conf",
            locations=[
                os.path.join("tests", "example_app", "deployable", "deploy", "roles", "application", "templates")
            ]
        )
        content = command.get_content()
        self.assertIsNone(content)

        # Exception is trapped.
        # self.assertRaises(TemplateNotFound, command.get_content)
        # with self.assertRaises(TemplateNotFound) as cm:
        #     command.get_content()

    def test_get_statement(self):
        deploy = Deploy("testing", "/var/www/example_app", "1.1", user="example_app")
        context = {
            'deploy': deploy,
            'domain_name': "example.app",
        }

        command = TemplateCommand(
            "httpd.conf.j2",
            "example.com.conf",
            context=context,
            locations=[
                os.path.join("tests", "example_app", "deployable", "deploy", "roles", "application", "templates")
            ]
        )
        statement = command.get_statement()
        self.assertTrue("cat > example.com.conf << EOF" in statement)
        self.assertTrue("example.app" in statement)
        self.assertTrue("example_app" in statement)

    def test_get_template(self):
        # First with a template that exists.
        command = TemplateCommand(
            "httpd.conf.j2",
            "example.com.conf",
            locations=[
                os.path.join("tests", "example_app", "deployable", "deploy", "roles", "application", "templates")
            ]
        )
        template = command.get_template()
        self.assertEqual("tests/example_app/deployable/deploy/roles/application/templates/httpd.conf.j2", template)

        # Next with a template that does not exist.
        command = TemplateCommand(
            "nonexistent.conf.j2",
            "example.com.conf",
            locations=[
                os.path.join("tests", "example_app", "deployable", "deploy", "roles", "application", "templates")
            ]
        )
        template = command.get_template()
        self.assertEqual("nonexistent.conf.j2", template)

    # TODO: Test _remote() execution of TemplateCommand.


class TestUsers(unittest.TestCase):

    def test_get_commands(self):
        c = AddUser("deploy")
        self.assertEqual(1, len(c.get_commands()))

        c = AddUser("deploy", home="/var/www/example_app")
        o = 'sudo adduser deploy --disable-password --gecos "" --home /var/www/example_app;'
        self.assertEqual(o, c.preview())

        c = AddUser("deploy", groups=["testing"])
        self.assertEqual(2, len(c.get_commands()))
        self.assertTrue("sudo adduser deploy testing;" in c.preview())

        c = AddUser("deploy", groups=["sudo", "testing"])
        self.assertEqual(4, len(c.get_commands()))
        self.assertTrue('sudo echo "deploy ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/deploy;' in c.preview())
