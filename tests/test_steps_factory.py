from deployable.steps.factory import factory, Config
import unittest

# Tests


class TestConfig(unittest.TestCase):

    def test_repr(self):
        """Test the representation of a command."""
        config = Config("/path/to/steps.cfg", role="testing")
        self.assertEqual("<Config testing>", repr(config))

    def test_load(self):
        config = Config("/path/to/nonexistent/steps.cfg")
        self.assertFalse(config.exists)

        self.assertFalse(config.load())
        self.assertFalse(config.is_loaded)

    # TODO: Test a config file where an environment has been specified for a step.
    # TODO: Test a config file where a step option has no value.


class TestFactory(unittest.TestCase):

    def test_no_mapping(self):
        command = factory("nonexistent")
        self.assertIsNone(command)

    def test_bad_step_signature(self):
        command = factory("mkdir")
        self.assertIsNone(command)
