from myninjas.context_managers import cd
import unittest
from deployable.utils import *


class Comparable(object):

    def __init__(self, name, environments=None, scope=None, tags=None):
        self.environments = environments or ["base"]
        self.name = name
        self.scope = scope or "deploy"
        self.tags = tags or list()

    def __eq__(self, other):
        return other.name == self.name


# Tests


class TestUtils(unittest.TestCase):

    def test_any_list_item(self):

        a = [1, 2, 3]
        b = [3, 4, 5]
        self.assertTrue(any_list_item(a, b))

        a = [1, 2, 3]
        b = [4, 5, 6]
        self.assertFalse(any_list_item(a, b))

        a = ["one", "two", "three"]
        b = ["three", "four", "five"]
        self.assertTrue(any_list_item(a, b))

        a = ["one", "two", "three"]
        b = ["four", "five", "six"]
        self.assertFalse(any_list_item(a, b))

        a = [
            Comparable("one", environments=["base"]),
            Comparable("two", environments=["base", "staging", "live"]),
            Comparable("three", environments=["base", "live"]),
        ]
        b = [
            Comparable("three", environments=["base"]),
            Comparable("four", environments=["base", "staging", "live"]),
            Comparable("five", environments=["base", "live"]),
        ]
        self.assertTrue(any_list_item(a, b))

        a = [
            Comparable("one", environments=["base"]),
            Comparable("two", environments=["base", "staging", "live"]),
            Comparable("three", environments=["base", "live"]),
        ]
        b = [
            Comparable("four", environments=["base"]),
            Comparable("five", environments=["base", "staging", "live"]),
            Comparable("six", environments=["base", "live"]),
        ]
        self.assertFalse(any_list_item(a, b))

    def test_expand_key_file(self):
        path = expand_key_file("/path/to/nonexistent")
        self.assertIsNone(path)

        with cd("tests/example_app"):
            path = expand_key_file("fake")
            self.assertEqual("deployable/keys/fake",  path)

    def test_filter_objects(self):
        # Environments.
        a = [
            Comparable("one", environments=["base"]),
            Comparable("two", environments=["base", "staging", "live"]),
            Comparable("three", environments=["base", "live"]),
        ]

        b = filter_objects(a, environments=["base"])
        self.assertEqual(3, len(b))

        b = filter_objects(a, environments=["live"])
        self.assertEqual(2, len(b))

        b = filter_objects(a, environments=["staging"])
        self.assertEqual(1, len(b))

        b = filter_objects(a, environments=["base", "staging"])
        self.assertEqual(3, len(b))

        b = filter_objects(a, environments=["base", "live"])
        self.assertEqual(3, len(b))

        # Scope.
        a = [
            Comparable("one"),
            Comparable("two", scope="deploy"),
            Comparable("three", scope="provision"),
            Comparable("four", scope="tenants"),
            Comparable("five", scope="all"),
        ]

        b = filter_objects(a)
        self.assertEqual(5, len(b))

        b = filter_objects(a, scope="deploy")
        self.assertEqual(3, len(b))

        b = filter_objects(a, scope="provision")
        self.assertEqual(2, len(b))

        b = filter_objects(a, scope="tenants")
        self.assertEqual(2, len(b))

        # Tags.
        a = [
            Comparable("one"),
            Comparable("two", tags=["common", "pgsql"]),
            Comparable("three", tags=["common", "application"]),
            Comparable("four", tags=None),
            Comparable("five", tags=["apache", "ssl"]),
        ]

        b = filter_objects(a, tags=None)
        self.assertEqual(5, len(b))

        b = filter_objects(a, tags=["common"])
        self.assertEqual(2, len(b))

        b = filter_objects(a, tags=["pgsql"])
        self.assertEqual(1, len(b))

        b = filter_objects(a, tags=["apache", "application"])
        self.assertEqual(2, len(b))


    def test_generate_random_password(self):
        s = generate_random_password()
        self.assertTrue(len(s) == 10)

        s = generate_random_password(length=17)
        self.assertTrue(len(s) == 17)
