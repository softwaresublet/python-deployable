import os
import unittest
from deployable.variables import factory, Variable

# TODO: Switch variable testing to example_app/ _variables.ini file.
EXAMPLES_PATH = os.path.join("tests", "examples")

# Tests


class TestVariable(unittest.TestCase):

    def test_eq(self):
        variable1 = Variable("test1", value="a")
        variable2 = Variable("test2", value="a")

        self.assertTrue(variable1 == variable2)

    def test_factory(self):
        context = {
            'testing': 123,
        }

        variables = factory(os.path.join(EXAMPLES_PATH, "_variables.ini"), context=context)
        self.assertEqual(5, len(variables))

    def test_repr(self):
        variable = Variable("testing", value=1)

        self.assertEqual("<Variable.deploy testing:1>", repr(variable))

    def test_str(self):
        variable = Variable("testing", value=1)

        self.assertEqual("1", str(variable))

        variable = Variable("testing")
        self.assertEqual("", str(variable))

    def test_type(self):
        variable = Variable("testing", value="yes")
        self.assertEqual(bool, variable.type)

        variable = Variable("testing", value=1)
        self.assertEqual(int, variable.type)

        variable = Variable("testing", value="abc")
        self.assertEqual(str, variable.type)